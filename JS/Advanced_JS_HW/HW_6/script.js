"use strict"
const url = 'https://api.ipify.org?format=json'

// async function info(ip) {
//     const myInfo = await fetch(`http://ip-api.com/json/${ip}`).then(res => res.json())
//     console.log(myInfo);
// }
document.querySelector('button').addEventListener('click', myIp)

async function myIp() {
    const {ip} = await fetch(url).then(res => res.json())
    const myInfo = await fetch(`http://ip-api.com/json/${ip}`).then(res => res.json())
    new UserData(myInfo).info()
    console.log(myInfo);
}

// myIp()

class UserData {
    constructor({timezone, country, region, city, regionName}) {
        this.timezone = timezone
        this.country = country
        this.region = region
        this.city = city
        this.regionName = regionName
    }

    info(){
        const body = document.querySelector('body')
        const div = document.createElement('div')
        div.innerHTML =
        `
        <p>${this.timezone}</p>
        <p>${this.country}</p>
        <p>${this.region}</p>
        <p>${this.city}</p
        <p>${this.regionName}</p>
        `
        body.append(div)
    }
}
