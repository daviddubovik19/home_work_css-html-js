// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// прототимне наслідування потрібно для того щоб роширити наша класс і добавить ще якісь параметри
// ---------------------------------------
// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// для звереня до нащадка та його параметров такі як(name, age, salary) або ще до  гетере та сетера


class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }

    get Name() {
        return this.name
    }

    set Name(value) {
        return this.name = value
    }

    get Age() {
        return this.age
    }

    set Age(value) {
        return this.age = value
    }

    get Salary() {
        return this.salary
    }

    set Salary(value) {
        return this.salary = value
    }


}

class Programmer extends Employee {
    constructor(name, age, salary,lang) {
        super(name, age, salary);
        this.lang = lang
    }
    get Salary() {
        return this.salary * 3
    }
}



const david = new Programmer('david', 18, 2000, 'UK')
const andre = new Programmer('andre', 17, 0, 'UK')
const nazar = new Programmer('nazar', 22, 20000, 'UK')


console.log(david.Salary)
console.log(andre)
console.log(nazar)



























































const unknown = new Programmer('שם נורא מאוד', 2200, 0, 'נו')
console.log(unknown)