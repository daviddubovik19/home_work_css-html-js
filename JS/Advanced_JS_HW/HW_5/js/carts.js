import elId from "./delet.js";

export class Carts {
    constructor(name, username, email, title, id) {
        this.name = name
        this.username = username
        this.email = email
        this.title = title
        this.id = id
    }

    create() {

        const container = document.querySelector('.container')
        const div = document.createElement('div')
        div.id = `post-${this.id}`
        div.innerHTML =
            `

        <h3> ${this.username}</h3>
        <h6>${this.name}</h6>
        <a href="#">${this.email}</a>
        <h4>${this.title}</h4>
        `
        const button = document.createElement('button')
        button.id = (`btn-${this.id}`)
        button.addEventListener('click', elId)

        div.append(button)
        container.append(div)

    }

    // button = document.querySelector('.closeModal')
    // button.addEventListener('click', elId(div.id))

}

