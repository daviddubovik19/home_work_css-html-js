'use strict';
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const elemDivs = document.createElement('div')
let body = document.querySelector('body')
elemDivs.id = `root`
body.append(elemDivs)

    books.forEach((el, index) => {
      try{
          let ul = document.createElement('ul')
          ul.innerHTML =
              `<li>${el.author}</li>
               <li>${el.name}</li>
               <li>${el.price}</li>`;

          if (!el.price){
              throw new Error(`Object Number ${index} don't have property price`);
          }
          if (!el.author){
              throw new Error(`Object Number ${index} don't have property author`);
          }
          if (!el.name){
              throw new Error(`Object Number ${index} don't have property name`);
          }
          elemDivs.append(ul)
      }catch (error){
          console.log(error.message)
      }
    });




























