'use strict'
const url = 'https://ajax.test-danit.com/api/swapi/films'


fetch(url)
    .then(response => response.json())
    .then(response => response.map((obj) => {
        const { name, episodeId, openingCrawl, characters, ...rest } = obj
         console.log(obj);
        // console.log(characters);
        const body = document.querySelector('body')
        const div = document.createElement('div')

        const result = characters.map((info) => {
          return fetch(info)
                .then(response => response.json())
        })
       const rel = Promise.all(result)
           .then(res =>{
             const  names = res.map(item =>{
                  return item.name
              })
               div.innerHTML = `
            <h2>${episodeId}</h2>
            <h3>${name}</h3>
             <p>${names.join()}</p>
            <p>${openingCrawl}</p>
            `
               body.append(div)
       })




    }))







