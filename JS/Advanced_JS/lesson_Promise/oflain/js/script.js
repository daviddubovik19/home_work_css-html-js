// // що таке promise це об'єкт- який обцяє повернути значення колись в майбуднему Асинхроний if/else
// const wring_out_time = 500;
// const squatting_tim = 200;
//
// function wringOut(count) {
//     return new Promise((resolve, reject) => {
//         if (count > 100) {
//             reject(new Error("слишком много отжиманий "))
//         }
//         setTimeout(() => {
//             resolve()
//         }, count * wring_out_time)
//     })
// }
//
// function squatting(count) {
//     return new Promise((resolve, reject) => {
//         if (count > 1000) {
//             reject(new Error("слишком много присиданий  "))
//         }
//         setTimeout(() => {
//             resolve()
//         }, count * squatting_tim)
//     })
// }
//
// // пример 1
// // console.log('начало тренеровки')
// // wringOut(101).then(() => {
// //     console.log('отжался 10 раз')
// //     return squatting(20)
// // }).then(() =>{
// //     console.log('присел 20 раз ')
// // }).catch((err) =>{
// //     console.log(err.toString())
// // })
// // пример 2 через async and await
// async function myTraining() {
//   try{
//       console.log('начало тренеровки')
//       await wringOut(10)
//       console.log('отжался 10 раз')
//       await squatting(20)
//       console.log('присел 20 раз ')
//       return true
//   } catch(err)  {
//     console.log(err.toString())
//       return false
// }
// }myTraining().then((resolve)=>{
//     console.log(resolve)
// })
//==============================================================
// const p = new Promise(function (resolve, reject) {
//     setTimeout(() => {
//         console.log('preparing data...')
//         const backendData = {
//             server: 'aws',
//             port: 2000,
//             status: 'working'
//         }
//         resolve(backendData)
//     }, 2000)
// })
// p.then(data => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             data.modified = true
//             resolve(data)
//         }, 2000)
//     })
// }).then(clientData => {
//     console.log('data received', clientData)
//     clientData.fromPromise = true
//     return clientData
// }).then(data => {
//     console.log('Modified',data )
// }).catch(err => console.error('Error: ',err) )
//     .finally(() => console.log('finally'))
// ==============================================
// const  sleep = ms => {
//    return  new Promise(resolve => {
//        setTimeout(()=> resolve(),ms)
//    })
// }
// // sleep(2000).then(()=> console.log('after 2 sec'))
// // sleep(3000).then(()=> console.log('after 3 sec'))
// // Promise.all([sleep(2000), sleep(5000)])
// //     .then(()=>{
// //         console.log('All promises')
// // })
//
// Promise.race([sleep(2000), sleep(5000)])
//     .then(()=>{
//         console.log('race promises')
//     })

//Асинхроность це запроси на сервер  =================================
// пример 1
// let a = 7
// setTimeout(()=>{
//     a = 99
//     console.log(a)
// },2000)
// console.log(a)

