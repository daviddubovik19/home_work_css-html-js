import getDoctorInfo from "../api/getDoctorInfo.js"
import Dentist from "../classes/Dantist.js";


const renderDoctors = async () => {
    const doctors = await getDoctorInfo();
    const mappedDoctors = doctors.map(() => {});

    mappedDoctors.foreach(() => {
        document.body.insertAdjacentHTML('beforeach', new Dentist().getDoctorHTML())
    })
}

export default renderDoctors;