export const capLetters = (string) => string.toUpperCase();
export const capFirstLetter = (string) => string.substr(0,1).toUpperCase() + string.substr(1);