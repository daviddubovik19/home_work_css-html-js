import {inputTypes} from "./inputTypes.js";

export default class Input{
    constructor(type,classlist,id) {
        this.type = type
        this.classList = classlist
        this.id = id
    }
    render(){
        if (inputTypes.includes(this.type) ){
            const  body = document.querySelector('body')
            const input = document.createElement('input')
            input.type = this.type
            input.classList = this.classList
            input.id = this.id
            body.append(input)
        }else {
            throw new Error('not is single input, вася')
        }

    }
}


// export  default Input