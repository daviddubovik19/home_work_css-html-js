# Search and filter

1. Получите данные по `https://murmuring-chamber-21831.herokuapp.com/posts`
2. Выведите все карточки на экран
3. Реализуйте функционал фильтрации постов через input и select `https://murmuring-chamber-21831.herokuapp.com/search?value={}`

P.S. Используйте модули, классы, и best practises
<hr />

Шаблон карточки:
```html

<div class="card">
    <p>${id}</p>
    <h3>${title}</h3>
    <p>${text}</p>
</div>

```
