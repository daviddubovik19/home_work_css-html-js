import requestOfIngridients from "./ingridient.js";
import getCoctail from "./getCoctail.js";
import Card from "./Card.js"

const select = document.getElementById('filter');

function renderSelect(selector, data) {
    let options = "";
    data.forEach(el => {
    options += `<option value="${el}">${el}</option>`;
    selector.innerHTML = options;
});

}

(async ()=> {
const data =  await requestOfIngridients()
renderSelect(select, data)
})();


select.addEventListener('change', async (event)=>{
    document.querySelector('.post-container').innerHTML = '';

    const value = await getCoctail(select.value);
    console.log(value);
    value.forEach((el)=>{
    console.log(el);
new Card(el).render(document.querySelector('.post-container'))
})
})


