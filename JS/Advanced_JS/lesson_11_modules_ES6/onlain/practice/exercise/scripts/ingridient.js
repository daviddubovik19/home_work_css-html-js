export default async function requestOfIngridients() {
const options = {
    method: 'GET',
    url: 'https://the-cocktail-db.p.rapidapi.com/list.php',
    params: {i: 'list'},
    headers: {
      'X-RapidAPI-Key': 'e106f45035msh7997dbc1dcfe014p16045ajsnfd118361c9c9',
      'X-RapidAPI-Host': 'the-cocktail-db.p.rapidapi.com'
    }
  };
  
  return await axios.request(options)
  .then( (res) => res.data.drinks.map((el)=>  Object.values(el)[0])
   ).catch(function (error) {
      console.error(error);
  });

}

  

  