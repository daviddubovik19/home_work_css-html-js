export default async function getCoctail(drink) {
    
const options = {
    method: 'GET',
    url: 'https://the-cocktail-db.p.rapidapi.com/filter.php',
    params: {i: drink},
    headers: {
      'X-RapidAPI-Key': 'e106f45035msh7997dbc1dcfe014p16045ajsnfd118361c9c9',
      'X-RapidAPI-Host': 'the-cocktail-db.p.rapidapi.com'
    }
  };
  
  return await axios.request(options)
  .then(response=> response.data.drinks)
  .catch(function (error) {
      console.error(error);
  });
  
}