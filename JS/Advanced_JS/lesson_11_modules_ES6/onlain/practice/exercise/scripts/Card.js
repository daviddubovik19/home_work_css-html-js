export default class Card {
    constructor({strDrink, strDrinkThumb, idDrink}) {
        this.title = strDrink;
        this.img = strDrinkThumb;
        this.id = idDrink;

    }
    render(selector) {
        const card = document.createElement('div');
        const {title, img, id} = this;
        card.id = id;
        card.innerHTML = ` 
        <h3>${title}</h3>
        <img src="${img}"/>`

        
        selector.append(card);
    }
}

