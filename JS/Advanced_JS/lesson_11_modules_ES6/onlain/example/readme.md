# ES6 modules

<blockquote>
По мере роста нашего приложения, мы обычно хотим разделить его на много файлов, так называемых «модулей». Модуль обычно содержит класс или библиотеку с функциями.
</blockquote>


<blockquote>
Долгое время в JavaScript отсутствовал синтаксис модулей на уровне языка. Система модулей на уровне языка появилась в стандарте JavaScript в 2015 году и постепенно эволюционировала. На данный момент она поддерживается большинством браузеров и Node.js.
</blockquote>
<hr />

## Что такое модуль?

Модуль – это просто файл. Один скрипт – это один модуль.

Модули могут загружать друг друга и использовать директивы export и import, чтобы обмениваться функциональностью, вызывать функции одного модуля из другого:

* `export` отмечает переменные и функции, которые должны быть доступны вне текущего модуля.
* `import` позволяет импортировать функциональность из других модулей.

```js

export const someFunction = () => {
    console.log('Hello from module')
}

```

```js

import { someFunction } from '../some-path.js'

someFunction();

```

## Всегда «use strict»

В модулях всегда используется режим `use strict`.

```js

a = 54;

const f = function(){
    console.log(this);
}

f();

```

## Своя область видимости переменных

Каждый модуль имеет свою собственную область видимости. Другими словами, переменные и функции, объявленные в модуле, не видны в других скриптах.

Модули должны экспортировать функциональность, предназначенную для использования извне. А другие модули могут её импортировать.

## Код в модуле выполняется только один раз при импорте

Если один и тот же модуль используется в нескольких местах, то его код выполнится только один раз, после чего экспортируемая функциональность передаётся всем импортёрам.Если один и тот же модуль используется в нескольких местах, то его код выполнится только один раз, после чего экспортируемая функциональность передаётся всем импортёрам.

## `import.meta`

Объект import.meta содержит информацию о текущем модуле.

Содержимое зависит от окружения. В браузере он содержит ссылку на скрипт или ссылку на текущую веб-страницу, если модуль встроен в HTML:

```js

console.log(import.meta.url)

```

## Модули являются отложенными ([deferred](https://developer.mozilla.org/ru/docs/Web/HTML/Element/script))

Другими словами: 

* загрузка внешних модулей, таких как `<script type="module" src="...">`, не блокирует обработку HTML.
* модули, даже если загрузились быстро, ожидают полной загрузки HTML документа, и только затем выполняются.
* сохраняется относительный порядок скриптов: скрипты, которые идут раньше в документе, выполняются раньше.


<hr />

# Экспорт и импорт

## Экспорт до объявления

Мы можем пометить любое объявление как экспортируемое, разместив export перед ним, будь то переменная, функция или класс.

```js

// экспорт массива
export let months = ['Jan', 'Feb', 'Mar', 'Apr', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

// экспорт константы
export const MODULES_BECAME_STANDARD_YEAR = 2015;

// экспорт класса
export class User {
  constructor(name) {
    this.name = name;
  }
}

```

## Экспорт отдельно от объявления

```js

// экспорт массива
let months = ['Jan', 'Feb', 'Mar', 'Apr', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

// экспорт константы
const MODULES_BECAME_STANDARD_YEAR = 2015;

// экспорт класса
class User {
  constructor(name) {
    this.name = name;
  }
}

export { months, MODULES_BECAME_STANDARD_YEAR, User };

```

## Импорт *

```js

import { months, MODULES_BECAME_STANDARD_YEAR, User } from './module.js'

console.log(months);
console.log(MODULES_BECAME_STANDARD_YEAR);
console.log(User);

```

Если импортировать нужно много чего, мы можем импортировать всё сразу в виде объекта, используя `import * as <obj>`.

```js

import * as example from './module.js'

console.log(example.months);
console.log(example.MODULES_BECAME_STANDARD_YEAR);
console.log(example.User);


```

## Импорт «как» и Экспорт «как»

```js

import { months as m, MODULES_BECAME_STANDARD_YEAR as year, User as UserClass } from './say.js';


console.log(m);
console.log(year);
console.log(UserClass);

```

```js

export { months as m, MODULES_BECAME_STANDARD_YEAR as year, User as UserClass } from './say.js';

```

## Экспорт по умолчанию

На практике модули встречаются в основном одного из двух типов:

1. Модуль, содержащий библиотеку или набор функций/
2. Модуль, который объявляет что-то одно.

Модули предоставляют специальный синтаксис `export default` для второго подхода.

```js

export default class User {
  constructor(name) {
    this.name = name;
  }
}

```

```js

class User {
  constructor(name) {
    this.name = name;
  }
}

export default User;

```

При импорте `export default`  фигурные скобки не нужны. Они используются только в случае именованных экспортов.
```js

import User from './module.js'

```

Так как в файле может быть максимум один export default, то экспортируемая сущность не обязана иметь имя.

```js

export default class {
  constructor(name) {
    this.name = name;
  }
}

```


## Имя «default»

```js

class User {
  constructor(name) {
    this.name = name;
  }
}

class Dog {
    constructor(dogName) {
        this.dogName = dogName;
    }
}

export { User as default, Dog };

```

```js

import User, { Dog } from './module.js'

console.log(User);
console.log(Dog);

```

## Реэкспорт

Синтаксис «реэкспорта» export ... from ... позволяет импортировать что-то и тут же экспортировать, возможно под другим именем.

```js

export {sayHi} from './say.js'; // реэкспортировать sayHi

export {default as User} from './user.js'; // реэкспортировать default

```


<hr />

# [Динамический импорт](https://learn.javascript.ru/modules-dynamic-imports)
