import axios from "axios";
// https://rapidapi.com/thecocktaildb/api/the-cocktail-db/


const options = {
    method: 'GET',
    url: 'https://the-cocktail-db.p.rapidapi.com/list.php',
    params: {i: 'list'},
    headers: {
      'X-RapidAPI-Key': 'e106f45035msh7997dbc1dcfe014p16045ajsnfd118361c9c9',
      'X-RapidAPI-Host': 'the-cocktail-db.p.rapidapi.com'
    }
  };
  
  axios.request(options).then(function (response) {
      console.log(response.data);
  }).catch(function (error) {
      console.error(error);
  });
  