// modules
// якщо ми хочему експортирувать перименую то запись експорта може бути двух видов
// периший вид
// Variables
export let one = 1;
// другий вид
let two = 2;
let three = 3
export {two, three};
// =======================================
// так же можна експортирувать класси та функції
//Class
 class Person{
    constructor(name) {
    this.name = name
    }
}
// function
 function sayHello() {
    console.log( 'hello')
}

export {Person,sayHello}
//rename
export {one as once, two as twice}; // с помощю "as" можна переназвать перимение





















