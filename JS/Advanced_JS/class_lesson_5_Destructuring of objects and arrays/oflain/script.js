'use strict'
// деструктуризация
// let [age, height, grade] = [16, 170, 10];
// console.log(age)
// console.log(height)
// console.log(grade)
// ====
// const destructObj = {name:'player1', x: 50, y: 44};
// const {name, x, y} = destructObj
// console.log(name, x, y)
// /==========
// let [age, height, ...rest] = [16, 170, 'asdf', 'sss', '123', 123]
// // console.log(age)
// // console.log(height)
// // console.log(rest)
// const numbers = [1, 2, 3, 99];
// const letter = ['a', 'b', 'c', 's'];
//
// const concat = [...numbers, ...letter];
// console.log(concat)
// ===========================
// как работать с масивами
// function calcValues(a, b) {
//     return [
//         a + b,
//         a - b,
//         a * b,
//         a / b
//     ]
// }
//
// const [sum, sub, mult, ...other] = calcValues(41, 10)
// // const sum = result[0];
// // const sub = result[1];
// // тоже самое
// // const [sum, sub] = result
// console.log(sum, sub, mult)
// как работать с обєкто
// const person = {
//     name: 'max',
//     age: 20,
//     address: {
//         country: 'Ukraine',
//         city: 'Kyiv'
//     }
// }
// // const name = person.name
// const {
//     name: fistName,
//     age,
//     car = 'машини нема',
//     address: {city, country}
// } = person
// console.log(fistName, age, car, address)


// оператори spread and rest ============
// работа оператора spread в масиве
// const citiesUkraine = ['Kherson','Kyiv','Lviv'];
// const  citiesEurope = ['Париж','Берлин','Прага'];

//Spread
// console.log(citiesUkraine)
// console.log(...citiesUkraine)
//
// console.log(citiesEurope)
// console.log(...citiesEurope)
// const allCities = [...citiesUkraine, 'Вашингтон', ...citiesEurope];
// console.log(allCities)
// работа оператора spread в об'єктами

// const citiesUkraineWithPopulation = {
//     Kyiv: 7,
//     Lviv: 10,
//     Kherson: 8
// }
// const citiesEuropeWithPopulation = {
//     Kyiv: 15,
//     Berlin:10,
//     Praha:3,
//     paris:2
// }
// console.log({...citiesUkraineWithPopulation})
// console.log({...citiesUkraineWithPopulation, ...citiesEuropeWithPopulation})
// console.log({...citiesEuropeWithPopulation, ...citiesUkraineWithPopulation})

// practice
// const numbers = [5, 37, 42, 17 ]
// console.log(Math.max(...numbers))
// const divs = document.querySelectorAll('div')
// const nodes = [...divs];
// console.log(nodes)


// Rest
// function Sum(a, b, ...rest) {
//     return a + b + rest.reduce((a, i) => a+ i, 0)
// }
//
// const numbers = [1, 2, 3, 4, 5]
// // spread!!!
// console.log(Sum(...numbers))
//
//


