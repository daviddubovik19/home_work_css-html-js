// класси======================================================================================================
// const animal = {
//     name:'Animal',
//     age: 5,
//     hasTail: true
// }
// тоже самое но через класс
// class Animal {
//     static type = 'ANIMAL'
//
//     constructor(options) {
//         this.name = options
//         this.age = options.age
//         this.hasTail = options.hasTail
//     }
//
//     voice() {
//         console.log('I am Animal!')
//     }
// }

// const animal = new Animal({
//     name: 'Animal',
//     age: 5,
//     hasTail: true
// })
// <------->
// class Component {
//     constructor(selector) {
//         this.$el = document.querySelector(selector)
//     }
//
//     hide() {
//     this.$el.style.display = 'none'
//     }
//
//     show() {
//         this.$el.style.display = 'block'
//     }
// }
//
// class Box extends  Component {
//     constructor(options) {
//         super(options.selector);
//         this.$el.style.width = this.$el.style.height = options.size + 'px'
//         this.$el.style.background = options.color
//     }
// }
//
// const  box1 = new  Box({
//     selector:'#box1',
//      size: 100,
//     color: 'red'
// })
// const  box2 = new  Box({
//     selector:'#box2',
//     size: 120,
//     color: 'blue'
// })
//
// class Circle extends Box{
//     constructor(options) {
//         super(options);
//
//         this.$el.style.borderRadius = '50%'
//
//     }
// }
// const  c = new Circle({
//     selector: '#circle',
//     size: 90,
//     color:'green'
//
//
// })
// <--------------------------------->


// class Task{
// constructor(title = Task.getDefaultTitle(),isCompleted = false) {
//     this.title = 'Lear ES6';
//     this._isCompleted = isCompleted
//     Task.counter += 1;
// }
// // статические свойства
//     static getDefaultTitle(){
//     return 'Title text';
//     }
// // методи ідуть після конструктора
//     completed (){
//     this.isCompleted = true;
//     }
//
//     get isCompleted(){
//     return this._isCompleted === true ? 'task is completed': 'task is not completed'
//     }
// }
// Task.counter = 0 //статическое свойство
// let task = new Task('Learn ES6', false);
// // --------------------------
// // let task2 = new Task('LEarn ReactJS', true)
// // let task3 = new  Task();
// // task.completed()
// // console.log( Task.counter)
// console.log(task.isCompleted, task._isCompleted)


// Prototype==================================================================================================
// const person = new Object({
//     name: 'Maxim',
//     age: 25,
//     greet: function() {
//         console.log('greed')
//     }
// })
//
// Object.prototype.sayHello = function (){ // таки образом ми раширили обєкт
//     console.log('Hello!')
// }
// const  lena = Object.create(person)  //таким образом ми создаем обєкт
// lena.name = 'Elena'
// const  str = new String('I am  string')
// наследування==================================================================================================
// class Task {
//     constructor(title = '') {
//         this.title = title,
//             this.isCompleted = false
//     }
//
//     completed() {
//         this.isCompleted = true;
//     }
// }
//
// class SubTask extends Task {
//     constructor(title) {
//         super(title);
//     }
//
//     completed() {
//         super.completed()
//         console.log(`task ${this.title} is completed!`)
//     }
// }
//
// let task = new Task('Learn ReactJS')
// let subTask = new SubTask('LEarn ES6')
// task.completed()
// subTask.completed()
// console.log(task)
// console.log(subTask)
// //class declaration
// class Task{};
// //class expression
// let Task = class Task {};

замикан
// ОПП=============================================================================================================
// так и нихуя не понял шо це