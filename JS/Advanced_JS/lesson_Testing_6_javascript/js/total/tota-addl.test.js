'use strict'
const add = require('./add')
const total = require('./total')
const sum = require("../sum/sum");

describe("testing total & add fn", () => {
    test('add', ()=>{
        expect(add(2,2)).toBe(4);
    });
    test('total', ()=>{
        expect(total(5,13)).toBe("$18");
    });
})
