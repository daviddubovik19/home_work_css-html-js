// AJAX, FETCH. AXIOS. JSON PARSE, STRINGIFY
// const requestURl = 'https://jsonplaceholder.typicode.com/users'
//
// function sendRequest(method, url, body = null) {
//     const headers = {
//         'Content-Type': 'application/json'
//     }
//     return fetch(ulr, { // fetch повертає promis первим параметром принимает  url
//
//         method: method,
//         body: JSON.stringify(body),
//         headers: header
//     }).then(response => {
//         if (response.ok) {
//             return response.json()
//         }
//         return response.json().then(error => {
//             const e = new Error('коду пезда ')
//             e.data = error
//             throw e
//         })
//     })
// }
//
// // sendRequest('GET',requestURl).then(data =>{
// //     console.log(data)
// // }).catch(err =>{
// //     console.log(err)
// // })
// const body = {
//     name: 'david',
//     age: 18
// }
// sendRequest('POST', requestURl, body).then(data => {
//     console.log(data)
// }).catch(err => {
//     console.log(err)
//
// })

// FETCH ========================================================
// без використання fetch
// let xhttp2 = new XMLHttpRequest()
// xhttp2.onreadystatechange = function () {
//     if (this.readyState == 4 && this.status == 200) {
//         callbackFunction(this.responseText)
//     }
// }
// xhttp2.open('GET', 'https://ajax.test-danit.com/api/swapi/films', true)
// xhttp2.send()
//
// function callbackFunction(data) {
//     console.log(data)
// }
// з використанням fetch
// fetch('https://ajax.test-danit.com/api/swapi/films')
//     .then(data => {
//         console.log(data)
//         // data.text().then(data2 =>{
//         //     console.log(data2)
//         // })
//         return data.text()
//     }).then(data => {
//     console.log(data)
// })
// =============================================================
fetch('https://ajax.test-danit.com/api/swapi/films', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: 'war' // body data type must match "Content-Type" header
}).then(response => response.text())
    .then(response => console.log(response))






