// AJAX, FETCH. AXIOS. JSON PARSE, STRINGIFY
const requestURl = 'https://jsonplaceholder.typicode.com/users'

function sendRequest(method, url, body = null) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open(method, url) // откривает запрос
        xhr.responseType = 'json' // ответ тип
        xhr.setRequestHeader('Content-Type','application/json')// устанавливаем хедери которие отправяца с запросом параметер це названме хедера друге це значение
        xhr.onload = () => { //слушатель
            if (xhr.status >= 400) {
                reject(xhr.response)
            } else {
                resolve(xhr.response)
            }
        }
        xhr.onerror = () => { //обратативает ошибки
            reject(xhr.onerror)
        }
        xhr.send(JSON.stringify(body)) //відпровляет запрос
    })
}

// sendRequest('GET',requestURl).then(data =>{
//     console.log(data)
// }).catch(err =>{
//     console.log(err)
// })
const body = {
    name: 'david',
    age: 18
}
sendRequest('POST', requestURl, body).then(data => {
    console.log(data)
}).catch(err => {
    console.log(err)

})







