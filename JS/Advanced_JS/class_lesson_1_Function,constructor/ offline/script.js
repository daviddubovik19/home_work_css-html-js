// конструктори
// що таке this це отчего имени визована функция"хто?"
// function Dog(name) {
//     this.myname = name;
//     this.voice  = function (){
//         console.log('hey, my name  is '+ this.myname)
//     }
// }
// let chauchau = new Dog("chapa");
// let buldog = new Dog("boxer");
// chauchau.voice();
// buldog.voice();
// оператор new заставляет функцию виполняться особими способом, условно говоря this контекс уже не будет контекстом функции

// <--------------------------------------------------------------->
//
// let Constr = function(name){
//   this.name = name;
//   this.canSpeak = true;
//   let priv = '34'//приватная переменая, її видно тільки в конструкторі
//   this.sayHello = function () {
//       return"привіт мене звати" + this.name + '. мне ' + priv;
//   }
// };
// let vanya = new Constr("ваня");
// console.log(vanya instanceof Constr); // instanceof проверяет наследие конструкта
// // console.log(vanya)
// // console.log(vanya.name)
// // console.log(vanya.canSpeak)
// console.log(vanya.sayHello())
//
// console.log(vanya.constructor) // свойство constructor показує весь конструктор
// <------------------------------------------------>
// контекст
function hello(){
    console.log('Hello', this)
}

const person ={
    name:'vladilen',
    age:25,
    sayHello: hello,
    sayHelloWindow: hello.bind(window), // метод bind() передает контекст которий привязан к цей функции
    logInfo: function(job, phone){
        console.group(`${this.name} info:`)// метод group() можно передать заголовок даной группи
        console.log(`Name is ${this.name}`)
        console.log(`Name is ${this.age}`)
        console.log(`job is ${job}`)
        console.log(` is ${phone}`)
        console.groupEnd()
    }
}

const lena = {
    name: 'elena',
    age: 23,
}
  // person.logInfo.bind(lena,'frontend','0999128950')()
// person.logInfo.call(lena, 'frontend','0999128950') // можем передать безкінечно пораметрів
person.logInfo.apply(lena,['frontend','0999128950'] ) // передаеться тільки два пораметра  и ми повині указать другий параметер в масиве

// або
// const fuLenaInfoLog = person.logInfo.bind(lena,'frontend','0999128950')()
// fuLenaInfoLog()
// або
// fuLenaInfoLog('frontend','0999128950')

// =====================================
const  array = [1,2,3,4,5]
// function multBy(arr,n){
// return arr.map(function (i){
// return i * n
// })
// }
Array.prototype.multBy = function (n){
    return this.map(function (i){
return i * n
})
}

console.log(array.multBy(2) )

