"use strict"
// AJAX - це асихроний javaScript і XML це методи які позволяють
// в фоновом режиме послать запрос на сервер
// с помощю javascripta получить результат ответа
// от сервера і працювати с нім.
// До цього момента ми могли только використовувать форми які перезагружають страницу або стороні програми
// XMLHttpRequest обєкт позволяет посилать запрос с помощю javascripta на сервер

// let xhttp = new XMLHttpRequest() // может посилать запроси на различние ресурси
// let a = 0
// xhttp.onreadystatechange = function () { // ця функция визиваеться всякий раз коли readyState меняет свое значение
//     if (this.readyState == 4 && this.status == 200) { // readyState  может принимать разние числовие значения
//         myFunction(this.responseText) // responseText це ответ на запрос  в виде строки або null в случае якщо запрос не успешен чи ответ еще не получив
//     }
// }
// xhttp.open('GET', 'https://ajax.test-danit.com/api/swapi/films', true) // open позволяет запустить запрос цей запрос требует по сути два параметра  способ запуска  і куда посилать  третий пареметер не обезательний но його часта прописуют цей параметер определяет как виполняеться запрос синхроно або асинхроно
// xhttp.send();
//
// function myFunction(data) {
//     a = data
//     console.log(data);
// }
// console.log(a)
// ===============
// let key = ['hello']
// let xhttp2 = new XMLHttpRequest()
// xhttp2.onreadystatechange = function () {
//     if (this.readyState == 4 && this.status == 200) {
//         myFunction2(this.responseText)
//     }
// }
// xhttp2.open('POST', 'https://ajax.test-danit.com/api/swapi/films', true)
// xhttp2.setRequestHeader('Content-type','application/x-www-form-urlencoded')
// xhttp2.send(key)//КЛЮЧ
// function myFunction2(data) {
//     console.log('POST');
//     a = data
//     console.log(data);
// }
//
// ===========================================================
// async and await

// const delay = ms => {
//     return new Promise((resolve, reject) => setTimeout(() => resolve(), ms))
// }
// const url = 'http://jsonplaceholder.typicode.com/todos'


// function fetchTodos() {
//     console.log('fetch todo started...');
//     return delay(2000)
//         .then(() =>  fetch(url)) // написав таким образо то поумолчанию якщо ми не пишем return то javascript автоматически вортає результат 
//          .then(resolve => resolve.json)
// }
// fetchTodos().then(data => { 
//     console.log(data); 
// }).catch(e =>console.log(e))

//  з використаням async and await
// async function fetchAsynctodos() {
//     console.log('fetch todo started...');
//     try {
//         await delay(2000);
//         const respose = await fetch(url);
//         const data = await respose.json;
//         console.log('Data:', data);
//     }catch (e){
//         console.error(e)
//     }finally {
//
//     }
//
// }
// fetchAsynctodos()
// =======================================================
// Асинхронность and Event Loop
// Асинхронность це коли якісь значение виводяться по заданому таймеру
// console.log('start')
// console.log('start2')
//
// function timeout2sec() {
//     console.log('timeout2sec')
// }
//
// window.setTimeout(() => {
//     console.log('inside timeout, after 5 seconds')
// }, 5000)
// setTimeout(timeout2sec, 2000)
// console.log('end')
//
//
// async function promiseArg(arg) {
//     return arg;
// }
//
// (async () => {
//     console.log(promiseArg(1))
//     console.log(await promiseArg(2))
//     console.log(await promiseArg(3))
//     console.log(promiseArg(4));
// })();