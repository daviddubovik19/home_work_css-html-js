


const input = document.getElementById("searchInput")
const res = document.querySelector(".result-output")
let url = new URL('https://movie-database-alternative.p.rapidapi.com/?r=json&page=1');

const options = {
    method: 'GET',
    headers: {
        'X-RapidAPI-Key': 'e106f45035msh7997dbc1dcfe014p16045ajsnfd118361c9c9',
        'X-RapidAPI-Host': 'movie-database-alternative.p.rapidapi.com'
    }
};

input.addEventListener("change", getResults);

function getResults(ev) {
    url.searchParams.set('s', ev.target.value)

    fetch(url, options)
        .then(function (resolve) {
            return resolve.json();
        })
        .then(function (result) {
            renderResults(result, res);
        })
}

function renderResults(data, container) {
    let total = [];
    container.innerHTML = "";
    data.Search.forEach(el => {
        // console.log(new Film (el).create());
        total.push(new Film (el).create());
    });
    console.log(total);
    container.append(...total)
}

class Film {
    constructor({ Poster, Title, Type, Year }) {
        this.title = Title
        this.type = Type
        this.year = Year
        this.poster = Poster
    }

    create() {
        const elem = document.createElement("div");
        elem.innerHTML = `
                <h3>${this.title}</h3>
                <h5>${this.type}</h5>
                <p>${this.year}</p>
                <img src="${this.poster}"/>
                `
        return elem;
    }
}


function postData(data){
    const headers = new Headers()
    headers.append("Content-Type", "application/json")

    const body = {
        "test": data
    }

    const options = {
        method: "POST",
        headers,
        mode: "cors",
        body: JSON.stringify(body),
    }

    fetch("https://eomgo7tsql564dq.m.pipedream.net", options).then(res=> console.log(res));
}

function getData(){
    fetch("https://eobf35odunyxuy4.m.pipedream.net")
        .then(r=>r.json())
        .then(result=>console.log(result.$return_value.event));
}

// postData({});
// getData();



