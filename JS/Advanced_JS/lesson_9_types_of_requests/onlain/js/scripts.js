//
"use strict";
//
// const input = document.getElementById("searchInput");
// const searchBtn = document.getElementById("searchBtn");
// const res = document.querySelector("#searchResults");
//
// let url = new URL('https://movie-database-alternative.p.rapidapi.com/?r=json&page=1');
//
// const options = {
//     method: 'GET',
//     headers: {
//         'X-RapidAPI-Key': 'e106f45035msh7997dbc1dcfe014p16045ajsnfd118361c9c9',
//         'X-RapidAPI-Host': 'movie-database-alternative.p.rapidapi.com'
//     }
// };
//
// searchBtn.addEventListener("click", getResults);
//
// function getResults() {
//     url.searchParams.set('s', input.value)
//
//     fetch(url, options)
//         .then(function (resolve) {
//             return resolve.json();
//         })
//         .then(function (result) {
//             renderResults(result, res);
//         })
// }
//
// function renderResults(data, container) {
//     console.log(data);
//     let total = [];
//     container.innerHTML = "";
//     data.Search.forEach(el => {
//         total.push(new Film (el).create());
//     });
//     container.append(...total);
// }
//
// function addBookmark(e, obj){
//     console.log(obj);
// }
//
//
//
//

//
//
//
// function postData(key, data){
//     const body = {
//         key: key,
//         data: data
//     }
//
//     const options = {
//         method: "POST",
//         headers: {
//             "Content-Type":"application/json"
//         },
//         mode: "cors",
//         body: JSON.stringify(body),
//     }
//
//     fetch("https://eomgo7tsql564dq.m.pipedream.net", options).then(res=> console.log(res));
// }
//
// function getData(){
//     fetch("https://eobf35odunyxuy4.m.pipedream.net")
//         .then(r=>{
//             //console.log(r)
//             return r.json();
//         })
//         .then(result=>console.log(result));
// }
//
// //postData('liubo-test2',{name: 'buuu'});
//
// //getData();
//
//
// function removeRecord(key){
//     const headers = new Headers()
//     headers.append("Content-Type", "application/json")
//
//     const body = {
//         key: key
//     }
//
//     const options = {
//         method: "DELETE",
//         headers,
//         mode: "cors",
//         body: JSON.stringify(body),
//     }
//
//     fetch("https://eomgo7tsql564dq.m.pipedream.net", options).then(res=> console.log(res));
// }
//
// //document.querySelector('#removeAll').addEventListener('click', ()=>removeRecord('liubo-test'));
// ==========================================================



"use strict";

const input = document.getElementById("searchInput");
const searchBtn = document.getElementById("searchBtn");
const res = document.querySelector("#searchResults");

let url = new URL('https://movie-database-alternative.p.rapidapi.com/?r=json&page=1');

const options = {
    method: 'GET',
    headers: {
        'X-RapidAPI-Key': 'e106f45035msh7997dbc1dcfe014p16045ajsnfd118361c9c9',
        'X-RapidAPI-Host': 'movie-database-alternative.p.rapidapi.com'
    }
};

searchBtn.addEventListener("click", getResults);

function getResults() {
    url.searchParams.set('s', input.value)

    fetch(url, options)
        .then(function (resolve) {
            return resolve.json();
        })
        .then(function (result) {
            console.log(result)
            renderResults(result, res);
        })
}

function renderResults(data, container) {
    console.log(data);
    let total = [];
    container.innerHTML = "";
    data.Search.forEach(el => {
        total.push(new Film (el).create());
    });
    container.append(...total);
}

function addBookmark(obj){
    console.log(obj);
    const {id, ...rest} = obj
    postData(id, {rest})
    //postData(filmId)

}

class Film {
    constructor({ Poster, Title, Type, Year, imdbID }) {
        this.title = Title
        this.type = Type
        this.year = Year
        this.poster = Poster
        this.id = imdbID
    }

    create() {
        const elem = document.createElement("div");
        elem.innerHTML = `
                <div class="row mb-2">
                    <div class="col-4">
                        <img src="${this.poster}" alt="${this.title}"/>
                    </div>
                    <div class="col-8">
                        <h4>${this.title}</h4>
                        <h5>${this.type}</h5>
                        <p>${this.year}</p>
                        <button type="button" class="btn btn-primary">Bookmark</button>
                    </div>
                </div>  
                `;
        elem.querySelector('.btn').addEventListener('click', () => addBookmark(this));
        return elem;
    }
}

function postData(key, data){
    const body = {
        key: key,
        data: data
    }

    const options = {
        method: "POST",
        headers: {
            "Content-Type":"application/json"
        },
        mode: "cors",
        body: JSON.stringify(body),
    }

    fetch("https://eomgo7tsql564dq.m.pipedream.net", options).then(res=> console.log(res));
}

function getPostedDate () {

    fetch('https://eobf35odunyxuy4.m.pipedream.net')
        .then(res => res.json())
        .then(res => console.log(res));


}

getPostedDate();





/*
function removeRecord(key){
    const headers = new Headers()
    headers.append("Content-Type", "application/json")

    const body = {
        key: key
    }

    const options = {
        method: "DELETE",
        headers,
        mode: "cors",
        body: JSON.stringify(body),
    }

    fetch("https://eomgo7tsql564dq.m.pipedream.net", options).then(res=> console.log(res));
}



*/






