"use strict";
// Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// для того, щоб виконати одну дію кілька разів

// <---------------------------------------------------------------------------------------->
// Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// коли треба перечислить числа от 0 до 100

// <---------------------------------------------------------------------------------------->
// Що таке явне та неявне приведення (перетворення) типів даних у JS?

// <---------------------------------------------------------------------------------------->

let userNumber = prompt("Number");

while (!userNumber || Number.isNaN(Number(userNumber))) {
  userNumber = prompt("Number");
}
if (userNumber < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
}
