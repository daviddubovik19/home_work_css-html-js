"use strict";

// Опишіть своїми словами що таке Document Object Model (DOM)

// <----------------------------------------------------------------------------->
// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML виводить строку с тегами,  innerText виводит только текст в теге
// <----------------------------------------------------------------------------->
// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// <----------------------------------------------------------------------------->

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const parag = document.querySelectorAll("p");
parag.forEach((item) => {
  item.style.backgroundColor = "#ff0000";
});

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const lisId = document.getElementById("optionsList");
console.log(lisId);
const bElement = document.body.closest('body');
console.log(bElement);
const doch = document.querySelectorAll("h2");
for (const iterator of doch) {
    console.log(iterator.childNodes);
}

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph
const idParag = document.getElementById("testParagraph");
idParag.textContent = 'This is a paragraph'
console.log(idParag);

// Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const getElement = document.getElementsByClassName("main-header");
console.log(getElement);

const changeСlass = (document.getElementsByClassName("main-header").className = "nav-item");

console.log(changeСlass);
const sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle);
for (const iterator of sectionTitle) {
    iterator.remove();
}

// <------------------------------------------------------------------>

// const teg = document.getElementsByTagName("");
//  for (const iterator of teg) {
//     teg.style.backgroundColor = "#ff0000";
//  }
