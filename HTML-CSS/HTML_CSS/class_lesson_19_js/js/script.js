"use strict";
/**
 * Задание 5.
 *
 * Написать функцию-помощник для ресторана.
 *
 * Функция обладает двумя параметрами:
 * - Размер заказа (small, medium, large);
 * - Тип обеда (breakfast, lunch, dinner).
 *
 * Функция возвращает объект с двумя полями:
 * - totalPrice — общая сумма блюда с учётом его размера и типа;
 * - totalCalories — количество калорий, содержащееся в блюде с учётом его размера и типа.
 *
 * Заметки:
 * - Дополнительные проверки совершать не нужно;
 * - В решении использовать референтный объект с ценами и калориями.
 */
// const priceList = {
//   sizes: {
//     small: {
//       price: 15,
//       calories: 250,
//     },
//     medium: {
//       price: 25,
//       calories: 340,
//     },
//     large: {
//       price: 35,
//       calories: 440,
//     },
//   },
//   types: {
//     breakfast: {
//       price: 4,
//       calories: 25,
//     },
//     lunch: {
//       price: 5,
//       calories: 5,
//     },
//     dinner: {
//       price: 10,
//       calories: 50,
//     },
//   },
//   getTotalPrice(size, type) {
//    return  this.sizes[size].price + this.types[type].price;
//   },
//   getTotalCalories(size,type) {
//     return this.sizes[size].calories + this.types[type].calories;
//   },
// };
// console.log(`общая сумма в калориях: ${priceList.getTotalCalories("small","breakfast")}`);
// console.log(`общая сумма в калориях: ${priceList.getTotalPrice("small","breakfast")}`);
// function restaurant(size, type) {
// let Price = this.sizes[size].price + this.types[type].price;
// let Calories = priceList.sizes[size].calories + priceList.types[type].calories;
//   return {
//     totalPrice: Price,
//     totalCalories: Calories,
//   };
// }
// <-----------------------------------------------------------------------.

/**
 * Задание 1.
 *
 * Написать функцию-фабрику, которая возвращает объекты пользователей.
 *
 * Объект пользователя обладает тремя свойствами:
 * - Имя;
 * - Фамилия;
 * - Профессия.
 *
 * Функция-фабрика в свою очередь обладает тремя параметрами,
 * которые отражают вышеописанные свойства объекта.
 * Каждый параметр функции обладает значением по-умолчанию: null.
 */

// const myObject = {
//   name: "david",
//   surname: "programe",
//   proffession: 'none,'

//     }

// function createUser(name = null, surname = null, prof = null) {
//   const user = {
//     name,
//     surname,
//     prof,
//   };
//   return user
// }

// console.log(createUser('Serhii','Siedov'));

// <------------------------------------------------------------------------------------->

/**
 * Задание 1.
 *
 * Создать объект пользователя, который обладает тремя свойствами:
 * - Имя;
 * - Фамилия;
 * - Профессия.
 *
 * Условия:
 * - Все свойства должны быть доступны только для записи;
 * - Решить задачу двумя способами.
 */

// const user = {
//   name: "Olga",
//   surname: "leus",
//   profession: "manager",
// };
// const defUser = {};
// Object.defineProperties(defUser, {
//   name: {
//     value: "olga",
//     writable: false,
//   },
//   surname: {
//     value: "leus",
//     writable: false,
//   },
//   proffession: {
//     value: "manager",
//     writable: false,
//   },
// });

// const user = {
//   _name: "Olga",
//   surname: "leus",
//   profession: "manager",

//   // set name(newValue) {
//   //   if(newValue.length > 3){
//   //     return (this._name = newValue);

//   //   }
//   get name() {
//     if(this._name.length > 3){
//       return this._name;

//     }
//   },
// };
// // console.log(user.name); - get
// user._name = "iri"; //-set
// console.log(user);

// <------------------------------------------------------------------------------------->

/**
 * Задание 2.
 *
 * Написать имплементацию метода Object.freeze().
 *
 * Метод должен:
 * - Предотвращать возможность добавления новых свойств к объекту;
 * - Предотвращать возможность удаления существующих свойств объекта;
 * - Предотвращать возможность изменения дескрипторов свойств объекта;
 * - Предотвращать возможность изменения значения свойств объекта.
 *
 * Условия:
 * - Встроенным методом Object.freeze() пользоваться запрещено;
 * - Встроенным методом Object.preventExtensions() пользоваться можно.
 */




const object = {
  name: "Anton",
  surName: "Kramarchuk",
  prof: "Killer",
};
function freezeObj(obj) {
  Object.preventExtensions(obj);
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      Object.defineProperty(obj, key, {
        writable: false,
        configurable: false,
      });
    }
  }
}
freezeObj(object);
object.name = "Karton";
delete object.surName;
object.age = 15;

console.log(object);
