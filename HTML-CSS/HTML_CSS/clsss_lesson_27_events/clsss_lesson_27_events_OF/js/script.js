"use strict";
// СОБИТИЯ
// по клику на кнопку виводит в консоль click
// const button = document.querySelector('.button')
// button.onclick = function() {
//     console.log('click');
// }
// второй метод
// function showConsole() {
//     console.log('click');
// }
// button.onclick = showConsole;
// <-------------------------------------------------------->
// нельзя повесить два обрабодчика на одно собития, но через addEventListener() можно повесить два обрабодчика на одно собитие
// const button = document.querySelector('.button')

// button.addEventListener('click', function(e) {
//     console.log('click');
// });
// button.addEventListener('click', function(e) {
//     console.log('click2');
// });
// можно винести в одельную функцию
// function showConsole(){
//     console.log('click');
//     button.removeEventListener('click',showConsole);
// }
// button.addEventListener('click',showConsole);
// button.removeEventListener('click',showConsole);
// <-------------------------------------------------------->

// опции
// const options = {
//   capture: false, // фазф,на которой должен сработать обрабодчик
//   once: false, // усли true, тогда обрабодчик будет автоматически удален после виполнения.
//   passive: false, // если true, то указивает, что обработчтк никогда не визовет preventDefault()
// };
// const button = document.querySelector(".button");

// function showConsole() {
//   console.log("Клик!");
// }
// button.addEventListener("click", showConsole, options);

// <-------------------------------------------------------->
// const button = document.querySelector(".button");

// function showConsole(event) {
//   // тип собития
//   // console.log(event.type);
//   //обєкт на котором сработал обрабодчик
//   console.log(event.target);
//   // обєкт к которому назначен обрабодчик
//   console.log(event.currentTarget);
//   //положение курсора по оси x
//   console.log(event.clientX);
//   //положение курсора по оси y
//   console.log(event.clienty);

//   //все детали собития
//   console.log(event);
// }

// button.addEventListener("click", showConsole);
// button.addEventListener('mouseenter', showConsole)

// <-------------------------------------------------------->
// для силок

// const link = document.querySelector('класс силки в html');

// link.addEventListener('click', function (event) {
//   console.log('наши действия');
//   //отменить действие браузера (переход по ссилке)
//   event.preventDefault();
// })
// <-------------------------------------------------------->

// простие собития
// самие часто используемие собития
/*
mousedown / mouseup - кнопка миши нажата / отпущена над елементом.
mouseover / mouseout - курсор миши появляеться над елементом и уходит с него 
mousemove - каждое движение миши над елемнтом генерирует це собитие.
contextmenu - визивается при попитке откритья контекстного меню,
ка правило, нажатие правой кнопки миши.
но, затем, це не совсем собитие миши, оно может визиваться и специальной клавишей клавеатури
*\
// комплексние собития

/*
click - визивается при mousedown, а затем mouseup над одним 
и тем же елементом, если использувалась основная кнопка миши.
dblclick - визивается двойним кликом на елементе.
 комплексние собития состоят из простих, поетому 
 в теории ми могли би без них обойтись.
 в теории ми могли би без них обойтись 
 но хорошо, что они существуют, потому что работать с ними очень удобно.
*\




