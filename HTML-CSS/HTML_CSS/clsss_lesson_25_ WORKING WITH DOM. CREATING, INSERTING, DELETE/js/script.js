"use strict";
/**
 * Задание 1.
 *
 * Написать скрипт, который создаст квадрат произвольного размера.
 *
 * Размер квадрата в пикселях получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в некорректном формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Все стили для квадрата задать через JavaScript посредством одной строки кода.
 *
 * Тип элемента, описывающего квадрат — div.
 * Задать ново-созданному элементу CSS-класс .square.
 *
 * Квадрат в виде стилизированного элемента div необходимо
 * сделать первым и единственным потомком body документа.
 */

// const div = document.createElement('div');
// div.classList.add('square');

// let elementSeze;
// do{
//     elementSeze = prompt("Введить число")
// }while(isNaN(elementSeze) || elementSeze <= 0);

// div.style.cssText = `width:${elementSeze}px;height:${elementSeze}px;background-color: red; margin: 300px auto;`;
// document.body.append(div);
// <------------------------------------------------------------------------->

/**
 * Задание 2.
 *
 * Написать функцию-фабрику квадратов createSquares.
 *
 * Функция обладает двумя параметром — количеством квадратов для создания.
 *
 * Если пользователь ввёл количество квадратов для создания в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Максимальное количество квадратов для создания — 10.
 * Если пользователь решил создать более 10-ти квадратов — сообщить ему о невозможности такой операции
 * и запрашивать данные о количестве квадратов для создания до тех пор, пока они не будут введены корректно.
 *
 * Размер каждого квадрата в пикселях нужно получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Цвет каждого квадрата необходимо запросить после введения размера квадрата в корректном виде.
 *
 * Итого последовательность ввода данных о квадратах выглядит следующим образом:
 * - Размер квадрата n;
 * - Цвет квадрата n;
 * - Размер квадрата n + 1;
 * - Цвет квадрата n + 1.
 * - Размер квадрата n + 2;
 * - Цвет квадрата n + 3;
 * - и так далее...
 *
 * Если не любом этапе сбора данных о квадратах пользователь кликнул по кнопке «Отмена»,
 * необходимо остановить процесс создания квадратов и вывести в консоль сообщение:
 * «Операция прервана пользователем.».
 *
 * Все стили для каждого квадрата задать через JavaScript за раз.
 *
 * Тип элемента, описывающего каждый квадрат — div.
 * Задать ново-созданным элементам CSS-классы: .square-1, .square-2, .square-3 и так далее.
 *
 * Все квадраты необходимо сделать потомками body документа.
 */
// // let squaresCount;
// do{
//     squaresCount = +prompt("squares Count")
//     if(squaresCount>10){
//         alert("squares Count must be less 11");
//     }
// }while(isNaN(squaresCount)||squaresCount<1||squaresCount>10)
// createSquares(squaresCount);

// function createSquares(count){
//     let squareSize;
//     let squareColor;

//     for (let i = 0; i < count; i++) {
//         do{

//             squareSize = prompt("squares size #" + (i+1));
//             if(squareSize===null){

//                 return console.log("Операция прервана пользователем.");
//             }

//         }while(isNaN(+squareSize)||+squareSize<0)
//         do{
//             squareColor = prompt("squares Color #" + (i+1));
//             if(squareColor===null){

//                 return console.log("Операция прервана пользователем.");
//             }

//         }while(!squareColor)
//         let square = document.createElement("div");
//         square.style.cssText = `width: ${squareSize}px; height: ${squareSize}px; background-color:${squareColor}`;
//         square.classList.add("square-"+ (i+1));
//         document.body.append(square);
//     }
// }

// // <---------------------------------------------------------------------------------------->
/**
 * Задание 3.
 *
 * Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
 *
 * Цвет тёмных ячеек — #161619.
 * Цвет светлых ячеек — #FFFFFF.
 * Остальные стили CSS для доски и ячеек готовы.
 *
 * Доску необходимо развернуть внутри элемента с классом .board.
 *
 * Каждая ячейка доски представляет элемент div с классом .cell.
 */

/* Дано */
const LIGHT_CELL = "#ffffff";
const DARK_CELL = "#161619";
const V_CELLS = 8;
const H_CELLS = 8;

let fillChessBoard = function () {
  const board = document.createElement("div");
  board.classList.add(".board");
  document.body.append(board);
  let isRowOdd = true;
  for (let i = 0; i < V_CELLS * H_CELLS; i++) {
    const cell = document.createElement("div");
    cell.classList.add("cell");
    board.append(cell);
    if (i % 8 === 0) {
      isRowOdd = !isRowOdd;
    }
    if (i % 2) {
      cell.style.backgroundColor = isRowOdd ? LIGHT_CELL : DARK_CELL;
    } else {
      cell.style.backgroundColor = isRowOdd ? DARK_CELL : LIGHT_CELL;
    }
  }
};

fillChessBoard();

