"use strict";
/**
 * ЗАДАНИЕ 1
 *
 * Вывести в консоль:
 * - Максимальное положительное число;
 * - Минимальное отрицательное число;
 * - Максимальное допустимое числовое значение в JavaScript;
 * - Минимальное допустимое числовое значение в JavaScript;
 * - Не число.
 *
 * Заметка: в этом задании необходимо проявить смекалку
 * и умение искать решения нестандартных задач.
 */
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Number

/* Максимальное доступное в языке число с плавающей точкой (дробное). */

// let maxNumber = Number.MAX_VALUE;
// let minNumber = Number.MIN_VALUE;
// let positivMax = Math.max(100, 200, -500);
// let negativeMin = Math.min(-500, -400, -1000);

// console.log(maxNumber);
// console.log(minNumber);
// console.log(positivMax);
// console.log(negativeMin);

// <-------------------------------------------------------------------------------------------------------->

/**
 * ЗАДАНИЕ 2
 *
 * Объяснить поведение каждой операции.
 */

//  const myNumber = 123;
//  const myString = '256';

//  console.log(myNumber + myString);

//  console.log('Разница: ' + (myNumber - myString));
//  console.log(myNumber * myString);

// <-------------------------------------------------------------------------------------------------------->

/**
 * ЗАДАНИЕ 3
 *
 * Объяснить поведение каждое операции.
 */

// let first = false;
// const second = true;

// console.log(first == 0);

// console.log(second === 1);
// <-------------------------------------------------------------------------------------------------------->

/**
 * ЗАДАНИЕ 5
 *
 * С помощью модальных окон получить от пользователя 3 числа и вывести в консоль их среднее арифметическое значение.
 *
 * Если в качестве одного из трёх значений пользователь ввёл не число — вывести сообщение:
 * «⛔️ Ошибка! Все три введённых значения должны быть числами!».
 * Среднее арифметическое значение в таком случае вычислять не нужно.
 */

// let a,d,c
//  a = +prompt("напешіть 1 числа");
//  b = +prompt("напешіть 2 числа");
//  c = +prompt("напешіть 3 числа");

//  if(isNaN(a) || isNaN(b) || isNaN(c)){
//     alert("⛔️ Ошибка! Все три введённых значения должны быть числами!")
//  }else{
//     alert((a + b + c)/3);
//  }

// <-------------------------------------------------------------------------------------------------------->

/**
 * Задание 5.
 *
 * Объяснить поведение каждое операции.
 */

//  let x, y, z;

//  x = 6;
//  y = 15;
//  z = 4;
//  console.log((x-- - y * 5));

//  console.log(--x);

//  x = 6;
//  y = 15;
//  z = 4;
//  console.log((y = y / (x + (5 % z))));

//  x = 6;
//  y = 15;
//  z = 4;
//  console.log((x -= -9));

//  console.log(!!'false' == !!'true');

//  console.log(!!'false' );

//  console.log(!!'true');

//  console.log('true' == true);

//  console.log('true' === true);

//  console.log(NaN == 1)
//  console.log(isNuN(NaN) == isNuN(NaN))
//  console.log(NaN === NaN);

//  console.log('1' == true)
//  console.log('2'+"2"-"2")

// <-------------------------------------------------------------------------------------------------------->

/**
 * Задание 6.
 *
 * Объяснить поведение каждое операции.
 */

//  console.log(true + false);
//  console.log(8 / "2");
//  console.log("number" + 5 + 1);
//  console.log(5 + 1 + "number");
//  console.log(null + 1);

//  console.log(undefined + 1);

// <-------------------------------------------------------------------------------------------------------->
/**
 * Задание 1.
 *
 * С помощью цикла вывести в консоль первые все нечётные числа,
 * которые находятся в диапазоне от 0 до 300.
 *
 * Заметка:
 * Чётное число — это число, которое делится на два.
 * Нечётное число — это число, которое не делится на два.
 *
 * Продвинутая сложность:
 * Не выводить в консоль числа, которые делятся на 5.
 */
// for (let i = 0; i < 300; i++) {
//   if (i % 2 == 0) {
//     console.log(i);
//   }
//   if (i % 3 == 0) {
//     console.log(i);
//   }
//   if (i % 5) {
//     console.log(i);
//   }
// }

// <-------------------------------------------------------------------------------------------------------->
/**
 * Задание 2.
 *
 * Пользователь должен ввести два числа.
 * Если введённое значение не является числом,
 * программа продолжает опрашивать пользователя до тех пор,
 * пока он не введёт число.
 *
 * Когда пользователь введёт два числа, вывести в консоль сообщение:
 * «Поздравляем. Введённые вами числа: «ПЕРВОЕ_ЧИСЛО» и «ВТОРОЕ_ЧИСЛО».».
 */
// <-------------------------------------------------------------------------------------------------------->
// Object

// const imgLink = "https://www.google.com/imgres?imgurl=https%3A%2F%2Fst.depositphotos.com%2F1006472%2F4058%2Fi%2F600%2Fdepositphotos_40582327-stock-photo-female-hands-opening-to-light.jpg&imgrefurl=https%3A%2F%2Fru.depositphotos.com%2Fstock-photos%2F%25D0%25BC%25D1%2583%25D1%2581%25D1%2583%25D0%25BB%25D1%258C%25D0%25BC%25D0%25B0%25D0%25BD%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B5.html&tbnid=wyBvCDt4R23RPM&vet=12ahUKEwiI_6zpi5T5AhUIuIsKHZhvDywQMygVegUIARCJAg..i&docid=8RtIB4oPBI42nM&w=600&h=600&q=%D0%BA%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%BA%D0%B0&client=opera-gx&ved=2ahUKEwiI_6zpi5T5AhUIuIsKHZhvDywQMygVegUIARCJAg";
// const title = "луна";
// const oldPrice = 22999;
// const newPrice = 19999;
// const colors = [" grey", "black"]

// const product = new Object;
//  let function contructor

// Object literal - {} - обиектний литерал , "" - строчний литерал, 242 -цифровой літерал
// const product = {
//       imgLink : "https://www.google.com/imgres?imgurl=https%3A%2F%2Fst.depositphotos.com%2F1006472%2F4058%2Fi%2F600%2Fdepositphotos_40582327-stock-photo-female-hands-opening-to-light.jpg&imgrefurl=https%3A%2F%2Fru.depositphotos.com%2Fstock-photos%2F%25D0%25BC%25D1%2583%25D1%2581%25D1%2583%25D0%25BB%25D1%258C%25D0%25BC%25D0%25B0%25D0%25BD%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B5.html&tbnid=wyBvCDt4R23RPM&vet=12ahUKEwiI_6zpi5T5AhUIuIsKHZhvDywQMygVegUIARCJAg..i&docid=8RtIB4oPBI42nM&w=600&h=600&q=%D0%BA%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%BA%D0%B0&client=opera-gx&ved=2ahUKEwiI_6zpi5T5AhUIuIsKHZhvDywQMygVegUIARCJAg",
//   title : "луна",
//  oldPrice : 22999,
//   _newPrice : 19999, //приватное свойство нижние подчеркивание
//   colors : [" grey", "black"],
//   set newPrice(value){
//     if(value > 0){
//         this._newPrice = value;
//     }
//   },
//   get newPrice(){
//     return this.newPrice
//   } ,
//   "is discount": true ,
//   setNewPrice(value){
//     if(value > 0) {
//         this._newPrice = value;
//     }

//   },
//   getPriceDiff: function(){
//     return this.oldPrice - this.newPrice
//   },
//   loggingThis(){
//     console.log(this);
//   },
// };

// product.setNewPrice(-1);
// console.log(product._newPrice);

// product.newPrice = 30000; //питаемся установить новое значение
// console.log(product.newPrice);

// // let priceDiff = product.getPriceDiff();
// // console.log(priceDiff);

// product.loggingThis();

//   свойства: значения свойства,  or  property: value, формула записи

// console.log(product);
// console.log(product.oldPrice); //оператор точка дає доступ до внутрішньої властивості об'єкта
// // другий різновид
// console.log(product["oldPrice"]);
// console.log(product["is discount"]);
// // добавиляем динамически  свойство
// product["top sales"] = true
// console.log(product);
// // удаляем свойство через оператор delete

// delete ["is discount"];
// console.log(product);
// // как проверить наличие свойства в об'єктe
// //  офрмат записи property in Object
// console.log("imgLink" in product); //true

// const product2 = {
//     imgLink ,
//     title,
//     oldPrice,
//     newPrice,
//      colors ,
// }
// console.log(product2);

// <-------------------------------------------------------------------------------------------------------->

/**
 * Задание 1.
 *
 * Создать объект пользователя, который обладает тремя свойствами:
 * - Имя;
 * - Фамилия;
 * - Профессия.
 *
 * А также одним методом sayHi, который выводит в консоль сообщение 'Привет.'.
 */

// const userName = {
//   name: "david",
//   surname: "dubovik",
//   profession: "programmer",
//   sayHi(){
//     console.log('привет'+ this.name);
//   },
// }
// let userName2 = Object.assign({},userName)

// userName2.name = " bogdan";

// userName2.sayHi();
// console.log(userName2);

// <-------------------------------------------------------------------------------------------------------->
/**
 * Задание 2.
 *
 * Расширить функционал объекта из предыдущего задания:
 * - Метод sayHi должен вывести сообщение: «Привет. Меня зовут ИМЯ ФАМИЛИЯ.»;
 * - Добавить метод, который меняет значение указанного свойства объекта.
 *
 * Продвинутая сложность:
 * Метод должен быть «умным» — он генерирует ошибку при совершении попытки
 * смены значения несуществующего в объекте свойства.
 */

// const user = {
//   name: "Olga",
//   "last name": "Leus",
//   profession: "manager",
//   sayHi(){
//       console.log("Hi, "+ this.name + " " + this["last name"] )
//   },
//   set(key, value){
//       if (key in this){
//           this[key] = value
//       }else{
//           alert (`${key} is not def.`)
//       }

//   }
// }

// console.log(user)
// user.set("name", "Oleg")
// user.set("profession", "student")
// user.set("surname", "Oleg")

// <-------------------------------------------------------------------------------------------------------->
/**
 * Задание 3.
 *
 * Расширить функционал объекта из предыдущего задания:
 * - Добавить метод, который добавляет объекту новое свойство с указанным значением.
 *
 * Продвинутая сложность:
 * Метод должен быть «умным» — он генерирует ошибку при создании нового свойства
 * свойство с таким именем уже существует.
 */

// const user = {
//   name: "Olga",
//   "last name": "Leus",
//   profession: "manager",
//   sayHi(){
//       console.log("Hi, "+ this.name + " " + this["last name"] )
//   },
//   set(key, value){
//       if (key in this){
//           this[key] = value
//       }else{
//           alert (`${key} is not def.`)
//       }

//   },
//   newProp(key, value){
//     if(key in this){
//       alert("свойство с такиим  именем существует ");
//     }else{
//       this[key] = value
//     }
//   }
// };

// user.newProp("age", 18);

// console.log(user)

// <-------------------------------------------------------------------------------------------------------->

/**
 * Задание 4.
 *
 * С помощью цикла for...in вывести в консоль все свойства
 * первого уровня объекта в формате «ключ-значение».
 *
 * Продвинутая сложность:
 * Улучшить цикл так, чтобы он умел выводить свойства объекта второго уровня вложенности.
 */

const myObject = {
  name: "david",
  prof: "prog",
  parents: {
    name: "zahar",
    prof: "бомж",
    parents: {
      name: "dimon",
      prof: "мусорщик",
    },
  },
};

function rec(Obj) {
  for (const key in Obj) {
    if (typeof Obj[key] !== "object") {
      console.log(`${key} = ${Obj[key]}`);
    } else {
      // це об'єкт
      // const sapObject = Obj [key]
      rec(Obj [key])
    }
  }
}

// <-------------------------------------------------------------------------------------------------------->
rec(myObject);
