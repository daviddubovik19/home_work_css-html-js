"use strict";

function generateFigure(size, color) {
  const newDiv = document.createElement("div");
  newDiv.classList.add("square");
  document.body.append(newDiv);
  newDiv.style.width = newDiv.style.height = size;
  newDiv.style.backgroundColor = color;
  if(getRandom((-1, 1))){
 newDiv.style.borderRadius = '50%'
  }
}

function getRandom(min, max) {
  return Math.ceil(Math.random() * (max - max) + min);
}

setInterval(() => {
  document.body.innerHTML = "";
  for (let i = 0; i < getRandom(1, 30); i++) {
    generateFigure(
      getRandom(10, 100) + "px",
      "#" +
        (Math.random().toString(16) + "000000").substring(2, 8).toUpperCase()
    );
  }
}, 1000);
function () {
    
}

    

