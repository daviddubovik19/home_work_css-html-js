"use strict";

// варіант 1
// const myArray = ["david","dva","obj"];
// for (let iterator of myArray) {
//     iterator = iterator.split("");
//     iterator[0] = iterator[0].toUpperCase();
//     iterator = iterator.join("");
//     console.log(iterator);
// }

// варіант 2
// const myArray = ["david","dva","obj"];
// for (let iterator of myArray) {
//     iterator = iterator.replace(iterator[0], iterator[0].toUpperCase());
//     console.log(iterator);
// }

// варіант 3
// const myArray = ["david", "dva", "obj"];
// console.log(
//   myArray.map(function (value) {
//     return value.replace(value[0], value[0].toUpperCase());
//   })
// );
// <---------------------------------------------------->

/**
 * Задача 1.
 *
 * Написать функцию-помощник кладовщика replaceItems.
 *
 * Функция должна заменять указанный товар новыми товарами.
 *
 * Функция обладает двумя параметрами:
 * - Имя товара, который необходимо удалить;
 * - Список товаров, которыми необходимо заменить удалённый товар.
 *
 * Функция не обладает возвращаемым значением.
 *
 * Условия:
 * - Генерировать ошибку, если имя товара для удаления не присутствует в массиве;
 * - Генерировать ошибку, список товаров для замены удалённого товара не является массивом.
 *
 * Заметки:
 * - Дан «склад товаров» в виде массива с товарами через.
 */

/* Дано */

// let storage = [
//       "apple",
//       "water",
//       "banana",
//       "pineapple",
//       "tea",
//       "cheese",
//       "coffee",
//     ];

//     function replaceItems(productToDelete, productsToInsert) {
//       if (!storage.includes(productToDelete)) {
//         alert("Ошибка - нету товара");
//         return;
//       }
//       if (!Array.isArray(productsToInsert)) {
//         alert("Ошибка - заменяемый товар не массив");
//         return;
//       }
//       storage.splice(storage.indexOf(productToDelete), 1, ...productsToInsert);
//     }

//     replaceItems("pineapple", ["lemon", "peach"]);
//     console.log(storage);

// <------------------------------------------------------------------------------->

// const arrayNew = ['vadim', 'zayats','kyiv' ]
// const myCallBackFunction = function (item, index) {
//     console.log(`${index} = ${item}`);
// };

// arrayNew.forEach(myCallBackFunction)

// function myForEach(a, b){
// for (let i = 0; i < a.length; i++) {
//     b(a[i], i)
// }
// }

// myForEach(arrayNew, myCallBackFunction)

// <------------------------------------------------------------------------------->

/**
 * Задача 2.
 *
 * Написать функцию-исследователь навыков разработчика developerSkillInspector.
 *
 * Функция не обладает аргументами и не возвращает значение.
 * Функция опрашивает пользователя до тех пор, пока не введёт хотя-бы один свой навык.
 *
 * Если пользователь кликает по кнопке «Отменить» в диалоговом окне, программа оканчивает
 * опрос и выводит введённые пользователем навыки в консоль.
 *
 *
 * Условия:
 * - Список навыков хранить в форме массива;
 * - В списке навыков можно хранить только строки длиной больше, чем один символ.
 */

// function developerSkillInspector() {
//   let question = "";
//   let getSkill = [];

//   while (question !== null) {
//     question = prompt("ввудите навік");
//     if(question !== null && question.length > 1){
//         getSkill.push(question);
//     }
//   }
//   console.log(getSkill);
// }

// developerSkillInspector();
// <------------------------------------------------------------------------------->

/**
 * Задача 3.
 *
 * Улучшить функцию-исследователь навыков разработчика из предыдущего задания.
 *
 * После ведения пользователем своих навыков функция выводит их на экран посредством функции alert.
 * После чего спрашивает правильно-ли пользователь ввёл данные о своих навыках.
 *
 * --- Если пользователь ответил «нет» ---
 * Программа спрашивает его какие навыки необходимо удалить из списка.
 * Если пользователь ввёл навык для удаления из списка, которого в списке не существует, программа
 * оповещает пользователя об ошибке и повторно запрашивает данные.
 *
 * Программа запрашивает данные о навыках для удаления из списка до тех пор,
 * пока пользователь не кликнет по кнопке «Отменить» в диалоговом окне.
 *
 * --- Если пользователь ответил «да» ---
 * Программа выводит данные о навыках в консоль.
 */

//  function developerSkillInspector(params) {
//     let question = "";
//     let getSkill = [];
//     while (question !== null) {
//       question = prompt("Введите навык");
//       if (question !== null && question.length > 1) {
//         getSkill.push(question);
//       }
//     }
//     alert("Ваши навыки: " + getSkill);
//     if (!confirm("Вы верно ввели навыки?")) {
//       let thirdQuestion;
//       while (
//         (thirdQuestion = prompt("Какой навыки необходимо удалить?")) !== null
//       ) {
//         if (getSkill.includes(thirdQuestion)) {
//           getSkill.splice(getSkill.indexOf(thirdQuestion), 1);
//         } else {
//           alert("Такого навыка не существует");
//         }
//       }
//     }
//     alert("Ваши навыки: " + getSkill);
//   }

//   developerSkillInspector();
// <------------------------------------------------------------------------------->

/**
 * Задача 1.
 *
 * Написать функцию-помощник покупателя.
 *
 * Функция не обладает параметрами и возвращает массив.
 * Функция запрашивает у пользователя строку с товарами, которые он желает приобрести.
 * Строка должна содержать все товары через запятую.
 *
 * Программа повторно опрашивает пользователя если:
 * - Введено менее чем один товар;
 * - Не введено товаров вообще.
 *
 * После чего программа выводит в консоль введённые пользователем товары,
 * при этом исключив из списка дубликаты.
 *
 * Условия:
 * - Обязательно использовать структуру данных Set.
 */

function buyHelper() {
  let products;
  do {
    products = prompt("what do you want ");
  } while (!products || products.split(",").length < 3);
let mySet = new Set(products.split(","));
console.log(Array.from(mySet));
}
buyHelper();