"use strict";
// простейший способ создать масив
// const arr = [1, "2", null, function () {}, [1, 2], { a: 1 }];

// console.log(arr);
// console.log(typeof arr);

// arr.push("to end "); //метод добовляет стоку "to end" в конец
// console.log(arr);

// arr.unshift("to start");  //метод добовляет строку "to start" в начало
// console.log(arr);

// arr.push("to end-1, to end-2 "); //можна добавить несколько елементов
// console.log(arr);

// <----------------------------------------->
// console.log(arr);
// arr.pop();   // метод < .pop > удаляет один  последний елемент  из масива
// console.log(arr);

// arr.shift(); // метод < .pop > удаляет первий елемент  из масива
// console.log(arr);
// <---------------------------------------------------->
// const str = "hello";

// const newArr = str.split("") //метод < .split >  розбевает сроку на масив
// console.log(newArr);

// const newStr = newArr.join("-") // метод < .join ()< можем передать сепаратор > об'єднує массив в строку
// console.log(newStr);
// <---------------------------------------------------->
// console.log(arr);
// arr.reverse
// console.log(arr); // переварачивает  наш массив

// console.log(arr.length);
// console.log(arr[4]);
// <---------------------------------------------------->
// for (let index = 0; index < arr.length; index++) {
//     console.log(arr[index]);
// }

// or

// for (const element of arr) {
//     console.log(element);
// }
// <---------------------------------------------------->

// const arr2 = [];
// console.log(arr2.length);

// arr2.length = 4;
// arr2.push(1, 2, 3, 4, 5)
// console.log(arr2);

// arr2[2] = 2;
// console.log(arr2);

// delete arr2[6];
// console.log(arr2);

// <---------------------------------------------------->
//МЕТОДІ МАССИВОВ

// const arr = [1, 2, 3, 4, 5, 6];

// console.log(arr.indexOf(3)); //вернет 2       // метод < .indexOf() >  ищет индекс елемента в масиве

// console.log(arr.indexOf(3, 3)); // -1 возрощаеться втом случае если елемент не був знайден
// console.log(arr.lastIndexOf(3, 3)); // виведе 2 // работает на оборот, ищет с конца масива
// console.log(arr.includes(3)); // возвращает true or false зависимости от того есть такое значение в массиве или нет
// // arr.concat([1, 2, 3, 4,], 7, 8, 9) // об'єднує наш массив с теми мвссивами которие мі передали в качестве аргументов ил с набором елементов
// console.log(arr.concat([1, 2, 3, 4,], 7, 8, 9));

// <---------------------------------------------------->
// методі для получение части массива
// console.log(arr.slice()); // виризает из массива кусак и возращяет нам
// console.log(arr.slice(2));
// console.log(arr.slice(2, 4));

// еще один метод которий ми можем использувать це метод .splice

// console.log(arr.slice()); // удаляет  из текущего массива елементи
// console.log(arr);

// console.log(arr.slice(2, 1));
// console.log(arr);
// <---------------------------------------------------------------->
// function callBackFn(){
// }
// function fnName(a, b, callBack){
//     a++;
//     a + b;
// callBack();
// }
// fnName(2, 3, callBackFn);
// <---------------------------------------------------------------->


// function arrCbFn(elValue, index, array){
//     console.log(`${index}: ${elValue} - ${array}`);
    
// }
// arr.forEach(arrCbFn);  // принимает callBackfn которую виполняет для каждогоелемента массива
// or
// arr.forEach((elValue, index, array)  => {
//     console.log(`${index}: ${elValue} - ${array}`);
// });// для перебора елементов в массиве

// <---------------------------------------------------------------->

// let  res = arr.find((elValue, index, array) => elValue > 4);    // метод < find > ищет елементи которие соответствуют условию колбек функции
// console.log(res);

// let  res = arr.findIndex((elValue, index, array) => elValue > 4);    // метод < find > ищет елементи которие соответствуют условию колбек функции
// console.log(res);

// res = arr.filter((elValue) => elValue > 4 );
// console.log(res);
// console.log(arr); //остался такимже

// <---------------------------------------------------------------->

// res = arr.map((value) => value * 4); // возрощает елементи нового массива 
// console.log(res);
// console.log(arr);


// <---------------------------------------------------------------->
// arr.reverse();
// console.log(arr);
// const arr2 = [1, 4, 8, 2, 5, 9];
// arr2.sort((a, b) => a - b); 
// console.log(arr2);

// arr.sort((a, b) => a - b); // сортирует массив
// console.log(arr); //  [1, 2, 3, 4, 5, 6]

// илиба 

// arr.sort((a, b) => b - a); 
// console.log(arr); // ( [6, 5, 4, 3, 2, 1]


// <---------------------------------------------------------------->
// const arr = [1, 2, 3, 4, 5, 6];
// res = arr.reduce(( startValue, currValue, currIndex, array) =>{ // метод < .reduce > находить суму значений 
//     return startValue + currValue ;
// }, 1);
// console.log(res);

// const arr = [1, 2, 3, 4, 5, 6];
// res = arr.reduce(( startValue, currValue, currIndex, array) =>{ // метод < .reduce > находить суму значений 
//     return startValue * currValue ;
// }, 1);
// console.log(res);


// <---------------------------------------------------------------->

// проверка на миссив

// let obj = {};
// let arr = [];

// console.log(typeof obj);
// console.log(typeof arr);

// // как нам узнать где массив а где нет ?

// if(Array.isArray(arr)){
//     console.log('це массив');
// }else{
//     console.log('це не массив');
// }


