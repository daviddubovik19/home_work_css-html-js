"use strict";

// получаем обєкти с html файла
// const htmlElement = document.documentElement;
// const headElement = document.head;
// const bodyElement = document.body
// console.log(htmlElement);
// console.log(headElement);
// console.log(bodyElement);
// <-------------------------------------------------->
// // получаем обькт body
// const bodyElement = document.body;
// // перви и последний дочерний елементи
// const firstChildNode = bodyElement.firstChild;
// const lastChildNode = bodyElement.lastChild;

// console.log(firstChildNode);
// console.log(lastChildNode);

// <-------------------------------------------------->

//навигация по докусенту

// получакм обькт body
// const bodyElement = document.body;
// коллекция childNodes содержит список всех детей
// включая текстовие узли
// const childNodes = bodyElement.childNodes;
// console.log(childNodes);

//для проверки наличия дочерних узлов
// существует также специальная фнкциия hasChildNodes();

// console.log(bodyElement.hasChildNodes());
/*как ми уже видели,
childNodes похож на массив. На самом деле це не массив, 
а кколлекция - особих перебираемій обьект - псевдомассив.

ОТличия от массивов:
1.для переьора коллекции міи можем использовать for..of:
2. Методі массивов не будут работать, потому чтот коллекция - це не массив
*/
//перебор коллекции
// for (const node of childNodes) {
//   console.log(node); //покажет все узли из коллекции 
// }
// <-------------------------------------------------->

// const childNodes = bodyElement.childNodes;
// console.log(childNodes);

// // получаем коллекцию всех дочерних елементов 

// const bodyChildren = bodyElement.children;
// console.log(bodyChildren);

// <-------------------------------------------------->

// методи поиска 
//  elem.querySelectorAll(CSS); он ищет м возрощает опсолютно все елементи в нутри некого обьекта которие удовлетворяют указаному ccs селектору 

// поиск по селектору класса
const elemsOne = document.querySelectorAll('.lesson__list')
console.log(elemsOne);

//поиск по селектору тега 
const elemsTwo = document.querySelectorAll('li');
console.log(elemsTwo);

//поис по смешанному селектору тега и класса 
const elemsThree = document.querySelectorAll('li.lesson__item-list');
console.log(elemsThree);

//поиск по тегу первого уровня вложенности 
const elemsFour = document.querySelectorAll('.lesson__list > li ');
console.log(elemsFour);

// поиск по нескольким классам  
const elemsFive = document.querySelectorAll('.lesson__list, .lesson__text ');
console.log(elemsFive);

//поиск по  вложенним классам
const elemsSex = document.querySelectorAll('.lesson__list .lesson__text');
console.log(elemsSex);

// поиск по ID
const elemsSeven = document.querySelectorAll('#listItem');
console.log(elemsSeven);

//поиск по атрибуту
const elemsEight = document.querySelectorAll('[data-item]');
console.log(elemsEight);

// поиск  по атрибуту со значением
const elemsNine = document.querySelectorAll('[data-item="85"]');
console.log(elemsNine);
// <-------------------------------------------------->

