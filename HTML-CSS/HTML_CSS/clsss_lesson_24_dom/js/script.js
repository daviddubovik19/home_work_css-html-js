"use strict";

/**
 * Задание 1.
 *
 * Получить и вывести в консоль следующие элементы страницы:
 * - По идентификатору (id): элемент с идентификатором list;
 * - По классу — элементы с классом list-item;
 * - По тэгу — элементы с тэгом li;
 * - По CSS селектору (один элемент) — третий li из всего списка;
 * - По CSS селектору (много элементов) — все доступные элементы li.
 *
 * Вывести в консоль и объяснить свойства элемента:
 * - innerText;
 * - innerHTML;
 * - outerHTML.
 */

//  const listD = document.getElementById("list");
//  console.log(listD);
 
//  const classD = document.getElementsByClassName("list-item");
//  for (const iterator of classD) {
//    console.log(iterator);
//  }
 
//  const teg = document.getElementsByTagName("li");
//  for (const iterator of teg) {
//    console.log(teg);
//  }
 
//  const cssLi = document.querySelector("li:nth-child(3)");
//  console.log(cssLi);
 
//  const cssLiAll = document.querySelectorAll("li");
//  console.log(cssLiAll);
 
//  console.log(cssLi.innerText);
 
//  console.log(cssLi.innerHTML);
 
//  console.log(cssLi.outerHTML);
// <-----------------------------------------------------------> 




/**
 * Задание 1.
 *
 * Получить элемент с классом .remove.
 * Удалить его из разметки.
 *
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах: в одну строку и в две строки.
 */

//  document.querySelector('remove').remove();
//  document.querySelector('bigger').classList.replace("bigger","active")
//  document.querySelector('bigger').classList.add("bigger","active")
//  document.querySelector('bigger').classList.replace("bigger")


// <--------------------------------------------------------------------------------->


/**
 * Задание 2.
 *
 * На экране указан список товаров с указанием названия и количества на складе.
 *
 * Найти товары, которые закончились и:
 * - Изменить 0 на «закончился»;
 * - Изменить цвет текста на красный;
 * - Изменить жирность текста на 600.
 *
 * Требования:
 * - Цвет элемента изменить посредством модификации атрибута style.
 */

// const products = document.querySelectorAll('.store > li ');
// console.log(products);
// for (const product of products) {
//     console.log(product.innerText);
//     let line = product.innerText.split(" ")[1];
//     if(line === '0'){
//         product.innerText = product.innerText.split(' ')[0] + " " + "закінчилось"
//         product.style.color = 'red';
//         product.style.fontWeight = '600';
//     }

// }
// <--------------------------------------------------------------->
/**
 * Задание 3.
 *
 * Получить элемент с классом .list-item.
 * Отобрать элемент с контентом: «Item 5».
 *
 * Заменить текстовое содержимое этого элемента на ссылку, указанную в секции «дано».
 *
 * Сделать это так, чтобы новый элемент в разметке не был создан.
 *
 * Затем отобрать элемент с контентом: «Item 6».
 * Заменить содержимое этого элемента на такую-же ссылку.
 *
 * Сделать это так, чтобы в разметке был создан новый элемент.
 *
 * Условия:
 * - Обязательно использовать метод для перебора;
 * - Объяснить разницу между типом коллекций: Array и NodeList.
 */



//  const targetElement =
//  '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';

// const listItem = document.getElementsByClassName('list-item');
// for (const iterator of listItem) {
//     console.log(iterator);
//     if(iterator.innerText == 'Item 5'){
//        iterator.innerHTML = targetElement
//     }else if (iterator.innerText == 'Item 6'){
//         iterator.innerHTML = targetElement
//     }
// }
// <------------------------------------------------------------------------>

/**
 * Задание 4.
 *
 * Написать программа для редактирования количества остатка продуктов на складе магазина.
 *
 * Программа должна запрашивать название товара для редактирования.
 * Если ввёденного товара на складе нет — программа проводит повторный запрос названия товара
 * до тех пор, пока соответствующее название не будет введено.
 *
 * После чего программа запрашивает новое количество товара.
 * После чего программа вносит изменения на веб-страницу: заменяет остаток указанного товара его новым количеством.
 */
 let productRemove = prompt('що замінити ?');
 while (!productRemove) {
     productRemove = prompt('що замінити ?');
 }
 function isProduct(productName) {
     let result = false;
     document.querySelectorAll('.store li ').forEach(function (element) {
         if(element.innerText.toLowerCase().starsWith(productName.toLowerCase() + ":")){
             result = true;
             }
     });
     
     return result;
 }
 let count = +prompt('нова кількість товару?')
 function changeCount(productName, newCount) {
     document.querySelectorAll('.store li ').forEach(function (element) {
         if(element.innerText.toLowerCase().starsWith(productName.toLowerCase()+ ":")){
             element.innerText =  productName + ": " + newCount;
             }
     });
 }
 changeCount(productRemove, count)