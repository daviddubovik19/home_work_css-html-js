"use strict";
// 1
// let sdvig = 0;
// let test = document.querySelector(".test");
// let times;
// // move();
// function move() {
//   test.style.marginLeft = sdvig + "px";
//   sdvig = sdvig + 10;
//   times = setTimeout(move, 50);
// }
// // let times = setInterval(move, 30 )
// document.querySelector(".stop").addEventListener("click", function () {
//   clearTimeout(times);
//   // clearInterval(times);
// });

// let a = 10;
// obr();
// function obr() {
//   document.querySelector(".out").innerHTML = a;
//   a--;
//   if (a < 0) {
//     clearTimeout(times);
//     alert("таймер отключен");
//   } else {
//     times = setTimeout(obr, 1000);
//   }
// }

// // setTimeout(move, 5000) // виполняет только один раз
// // или
// // setInterval(() => { // виполняет несколько раз
// //     move()
// // }, 30);
// <--------------------------------------------------------------->
// 2
// localStorage
window.onload = function () {
  if (localStorage.getItem("bgcolor") !== null) { //  проверяем что лежит в localStorage.getItem("bgcolor") и применяем к body
    let color = localStorage.getItem("bgcolor");
    document.getElementsByTagName("body")[0].style.background = color;
  }
  document.querySelector(".green").addEventListener("click", () => {
    document.getElementsByTagName("body")[0].style.background = "green";
    localStorage.setItem("bgcolor", "green");
  });
  document.querySelector(".red").addEventListener("click", () => {
    document.getElementsByTagName("body")[0].style.background = "red";
    localStorage.setItem("bgcolor", "red");//  записую в localStorage  а  setItem позволяет витащить по имени 
  });
};
