'use strict';

const gulp = require('gulp'), pug = require('gulp-pug');

gulp.task('pug', function (){
    return gulp.src('src/pug/pages/*.pug')
        .pipe(pug({ // обрабодчик gulp,позволяет запускать плагини
            pretty:true // если не укозать true то файл будет конвертируеться в одну строку НЕ ЗАБИТЬ УКАЗАТЬ !!!
        }))
        .pipe(gulp.dest('build')); // dest це переменая которой указують папку в которую все будет складиваться
})