import * as http from 'http'
import * as fs from 'fs'
import * as mime from 'mime-types'

const server = http.createServer((req, res) => {

    let filePath = getFilePath(req.url);

    fs.readFile(filePath, (err, data) => {

      const mimeType =  mime.lookup(filePath.split('.').pop());
        res.writeHead(200,{
            'Content-Type': mimeType
        })
        if (err) {
            res.writeHead(404,{
                'Content-Type': mimeType
            });
        }
        res.end(err ? '404: ' + err.message : data);
    })
})

server.listen(3000, () => {
    console.log('server started')
})
let getFilePath = (url) => {
    let fileName = url.length > 1 && url.includes('.')
        ? url.substring(1)
        : '/index.html';
    return 'site/' + fileName;

}


console.log('http//localhost:3000')
