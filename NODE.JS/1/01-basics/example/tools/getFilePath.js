let path = require('path');

let getFilePath = (url) => {
    let fileName = url.length > 1 && url.includes('.')
        ? url.substring(1)
        : 'index.html';
    return path.resolve(__dirname, '../app', fileName);
}

module.exports = getFilePath;