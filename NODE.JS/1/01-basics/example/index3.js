let http = require('http');
let fs = require('fs');

let app = http.createServer((req, res) => {
    console.log('Responding to a request.');

    let filePath = getFilePath(req.url);

    fs.readFile(filePath, (err, data) => {
        // if(err) {
        //     res.end('404: ' + err.message);
        // } else {
        //     res.end(data);
        // }
        res.end(err ? '404: ' + err.message : data);
    });

});

let getFilePath = (url) => {
    let fileName = url.length > 1 && url.includes('.html')
        ? url.substring(1)
        : '/index.html';
    return 'app/' + fileName;
}


app.listen(3000);
console.log('Server works on http://localhost:3000');