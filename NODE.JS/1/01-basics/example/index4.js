let http = require('http');
let fs = require('fs');
let getFilePath = require('./tools/getFilePath');

let app = http.createServer((req, res) => {
    console.log('Responding to a request.');

    let filePath = getFilePath(req.url);

    fs.readFile(filePath, (err, data) => {
        if(err) {
            res.writeHead(404);
        }
        res.end(err ? '404: ' + err.message : data);
    });

});


app.listen(3000);
console.log('Server works on http://localhost:3000');