let http = require('http');
let fs = require('fs');

let app = http.createServer((req, res)=>{
    console.log('Responding to a request.');

    let url = req.url;
    switch (url) {
        case '/':
        case '/index.html':
            fs.readFile('app/index.html', (err, data) => {
                res.end(data);
            });
            break;
        case '/contacts.html':
            fs.readFile('app/contacts.html', (err, data) => {
                res.end(data);
            });
            break;
        default:
            res.end('404: page not found!');
    }

});

app.listen(3000);
console.log('Server works on http://localhost:3000');