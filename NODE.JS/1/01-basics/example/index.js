let http = require('http');
let fs = require('fs');

let app = http.createServer((req, res)=>{
    console.log('Responding to a request.');
    //res.end('<h1>Hello, World!!!</h1>');
    fs.readFile('app/index.html', (err, data) => {
        res.end(data);
    });
});

app.listen(3000);
console.log('Server works on http://localhost:3000');