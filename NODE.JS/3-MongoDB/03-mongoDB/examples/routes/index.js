// const addPost = require('../modules/Post/add');
// const getPost = require('../modules/Post/get');
// const updatePost = require('../modules/Post/update');
// const deletePost = require('../modules/Post/delete');
//
// module.exports = (app, db) => {
//     addPost('../Controllers/Post/add')(app, db);
//     getPost('../Controllers/Post/get')(app, db);
//     updatePost('../Controllers/Post/update')(app, db);
//     deletePost('../Controllers/Post/delete')(app, db);
// }

// module.exports = (app, db) => {
//     require('../Controllers/Post/add')(app, db);
//     require('../Controllers/Post/get')(app, db);
//     require('../Controllers/Post/update')(app, db);
//     require('../Controllers/Post/delete')(app, db);
// }

const fs = require('fs');
const path = require('path');

module.exports = (app, db) => {
    fs.readdir(path.join(__dirname, '../Controllers/Post/'), (err, files) => {
        files.forEach((file)=> {
            require('../Controllers/Post/' + file)(app, db);
        })
    })
}