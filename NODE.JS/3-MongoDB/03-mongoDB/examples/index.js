const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const dbConfig = require('./config/db');

app.use(express.urlencoded());

MongoClient.connect(dbConfig.url, {useNewUrlParser: true}, (err, client) => {
    if(err) return console.log(err.message);

    const db = client.db();

    const allRouts = require('./routes');
    allRouts(app, db);
    ///require('./routes')(app, database);

    app.listen(3000, () => {
        console.log("connected to " + db.url + ' ' + db.dbName);
        console.log("Server running on http://localhost:3000 ");
    })
});


