const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {
    app.get('/all',async (req, res) => {

        let Posts = [];

        try {
            await db.collection('posts').find().forEach(element => {
                Posts.push(element)
            })

            res.send(JSON.stringify(Posts));
        } catch (err) {
            res.send(err.message);
        }

    }).get('/post/:id', async (req, res) => {
        //res.send(req.params.id);
        let query = {_id: ObjectId(req.params.id)};

        try {
            const result = await db.collection('posts').findOne(query)
            res.send(result)
        } catch (err) {
            res.send(err.message);
        }

    })
}