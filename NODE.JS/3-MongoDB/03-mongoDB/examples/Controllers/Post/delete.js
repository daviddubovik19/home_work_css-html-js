const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {
 app.route('/delete/:id')
     .get((req, res) => {
         res.send("<form action='' method='post'>Are you sure? <input type='submit'></form>")
     })
     .post(async (req, res) => {
        let query = {_id: ObjectId(req.params.id)};

        try {
            await db.collection('posts').deleteOne(query);
            res.send('Post with id ' + req.params.id + ' successfully deleted!')

        } catch (err) {
            res.send(err.message)
        }
     })
}