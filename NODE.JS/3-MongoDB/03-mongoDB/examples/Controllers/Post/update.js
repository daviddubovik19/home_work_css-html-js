const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {

    app.route('/update/:id')
        .get(async (req, res) => {

            let query = {_id: ObjectId(req.params.id)};
            let result = {};
            try {
                result = await db.collection('posts').findOne(query)
            } catch (err) {
                res.send(err.message);
            }

            res.send("<form action='' method='post'>" +
                "<input type='text' name='title' value='" + result.title +"'>" +
                "<textarea name='text'>" + result.text + "</textarea>" +
                "<input type='submit'></form>")
        })
        .post(async (req, res) => {
            let query = {_id: ObjectId(req.params.id)};

            const infoForUpdate = req.body;

            try {
                await db.collection('posts').updateOne(query, {$set: infoForUpdate}, {upsert: true});
                res.send('Post with id ' + req.params.id + ' successfully updated!')

            } catch (err) {
                res.send(err.message)
            }
        })
}