const fs = require('fs');
const path = require('path')

module.exports = (app, db) => {
    app.route('/add')
        .get((req, res)=>{
            let form = path.resolve(__dirname, './', '../../templates/form.html');
            fs.readFile(form, (err, data) => {
                if(err) res.send(err.message);

                res.setHeader('Content-type', 'text/html')
                res.send(data);
            })
        })
        .post(async (req, res)=>{
            const post = {
                title: req.body.title,
                text: req.body.text,
            }

            let result = {};

            try {
                result = await db.collection('posts').insertOne(post);
            } catch (err) {
                res.send(err.message);
            }

            res.send('Post successfully added: <b>' + result.insertedId + '</b>')
        })
}