const express = require('express')
let app = express()
let ejs = require('ejs')
let morgan = require('morgan')
const fs = require("fs");
const path = require("path");
const cors = require('cors');

const accessLogStream = fs.createWriteStream(path.join(__dirname, "access.log"), {flags: 'a'})
app.use(morgan("combined", {stream: accessLogStream}));

app.set("views", "./views");
app.set("view engine", "ejs")
app.use(cors());

app.use(express.urlencoded()); // для form

app.route('/')
    .get(function (req, res) {
        res.send(`<form method='post'><input name='username'/>
            <button type='submit'>submit</button> </form>`)
    })
    .post(function (req, res) {
        res.send('Form is submitted: ' + req.body.username);
    })

app.get('/aboutus', (req, res) => {
    res.render('aboutus',{ name: "Ejs Title", userName:'david'})
});

app.listen(3000, () => {
    console.log('Server works on http://localhost:3000');
});