const express = require('express');
const app = express();

// let bodyParser = require('body-parser');

// app.use(bodyParser.json());
// app.use(express.json());
// app.use(bodyParser.urlencoded({extended: false}));



const cors = require('cors');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');

const accessLogStream = fs.createWriteStream(path.join(__dirname, "access.log"), {flags: 'a'})

app.use(morgan("combined", {stream: accessLogStream}));

app.use(express.urlencoded());
app.use(cors());

app.use('/templates', express.static('public'));

// app.engine("pug", require('pug').__express);
app.set("views", "./views");
app.set("view engine", "ejs")


app.get('/', (req, res) => {
    res.send('Home page <img src="/templates/img/full_page.jpg">');
});

app.get('/page/:id', (req, res) => {
    res.send('Page ID: ' + req.params.id);
})

app.route('/contacts')
    .get((req, res) => {
        res.send("<h1>Contacts: </h1>" +
            "<form method='post'><input name='username'><input type='submit'></form>")
    })
    .post((req, res)=> {

        res.send('Form is submitted: ' + req.body.username);
    });

app.get('/aboutus', (req, res) => {
    res.render('aboutus',{ name: "Pug Title"})
});

app.listen(3000, ()=> {
    console.log('Server works on http://localhost:3000');
});
