import {square} from "./square";

describe('square', () => { //якщо ви хочите описати декілька сценаріїв

    test('коректное значение', () => {
        // expect(square(2)).toBe(4)
        // expect(square(2)).toBeLessThan(5) // перевіряє що число яке ми получили меньше 5
        // expect(square(2)).toBeGreaterThan(3) // перевіряє що число яке ми получили більше 3
        // expect(square(2)).not.toBeUndefined() //перевіряє що возращаемое значение не Undefined
        // <=======================================>
        const spyMathPow = jest.spyOn(Math,'pow')
        square(2)
        expect(spyMathPow).toBeCalledTimes(1)
        // <=======================================>

    });
    test('коректное значение', () => {

        const spyMathPow = jest.spyOn(Math,'pow')
        square(1)
        expect(spyMathPow).toBeCalledTimes(0)
    });
    afterEach(()=>{
        jest.clearAllMocks()
    })
})