import testFunk from "./App";


    test('test App', () => {
        expect(testFunk(50)).toBe(true)
    });

describe('box test App',()=>{ //якщо ви хочите описати декілька сценаріїв
    test('коректное значение', () => {
        expect(testFunk(50)).toBe(true)
    });
    test('меньше коректности', () => {
        expect(testFunk(-1)).toBe(false)
    });
    test('больше коректности ', () => {
        expect(testFunk(101)).toBe(false)
    });
    test('пограничное значение снизу ', () => {
        expect(testFunk(0)).toBe(true)
    });
    test('пограничное значение сверху', () => {
        expect(testFunk(100 )).toBe(true)
    });
})