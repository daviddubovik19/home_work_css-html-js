import mapArrToString from "./mapArrToString";


describe('box test mapArrToString', () => { //якщо ви хочите описати декілька сценаріїв
    test('коректное значение', () => {
        expect(mapArrToString([1, 2, 3])).toEqual(['1','2','3']) // метод toEqual перевіряє що об'єкти однакові
    });
    test('Мешанина', () => {
        expect(mapArrToString([1, 2, 3, null,undefined,'asfasf'])).toEqual(['1','2','3'])
    });
    test('empty Array', () => {
        expect(mapArrToString([])).toEqual([])
    });
    test('отрицание', () => {
        expect(mapArrToString([1, 2, 3,])).not.toEqual([1, 2, 3,4])
    });



})

