import {RouterProvider} from "react-router-dom";
import Loader from "./components/Loader";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";

import router from "./routes"
import store, {persistor} from "./store";
import {useState} from "react";

import ThemeContext from "./context";

function App() {

    const setColor = (theme) => {
        localStorage.setItem('theme', JSON.stringify(theme));
        setTheme(theme);
    }
    const [theme, setTheme] = useState(
        localStorage.getItem('theme') ?
            JSON.parse(localStorage.getItem('theme')) :
            {color: 'red'}
    );

    return (
        <>
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <ThemeContext.Provider value={{theme, setColor}}>
                        <RouterProvider router={router} fallbackElement={<Loader/>}/>
                    </ThemeContext.Provider>
                </PersistGate>
            </Provider>
        </>
    );
}

export default App;
