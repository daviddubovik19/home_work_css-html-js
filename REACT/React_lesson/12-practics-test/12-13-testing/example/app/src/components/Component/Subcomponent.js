import React from 'react';

const Subcomponent = ({count, handlerCounter}) => {
    return (
        <div>
            <h1 role='counter'>clicked {count}</h1>
            <button onClick={handlerCounter}>IncrementButton</button>
        </div>
    );
};

export default Subcomponent;