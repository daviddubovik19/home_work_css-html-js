import {useState} from "react";
import Subcomponent from "./Subcomponent";
import {useSelector} from "react-redux";

export default function () {
    const [count, setCount] = useState(0);

    const status = useSelector(state => state.clicker.clicked);

    const handlerCounter = () => {
        setCount(count => count + 1);
    }

    return (
        <>
            <p>status from Redux: {status}</p>
            <Subcomponent count={count} handlerCounter={handlerCounter}></Subcomponent>
        </>
    )
}