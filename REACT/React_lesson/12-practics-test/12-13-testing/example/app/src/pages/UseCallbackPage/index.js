import SubComponent from "./SubComponent";
import {useCallback, useState} from "react";

export default function () {

    // const someHandler = () => {
    //     console.log('someHandler');
    // }

    const someHandler = useCallback(()=> {
        console.log('someHandler');
    },[]);

    const [count, setCount] = useState(0);
    console.log('UseCallbackPage render');

    return (
        <>
        <h1>useCallback example</h1>

            <h1>Memo</h1>
            <h2>Clicked times: {count}</h2>
            <button onClick={() => {
                setCount((count) => {
                    return count + 1
                })
            }}>click
            </button>

            <SubComponent someHandler={someHandler}></SubComponent>
        </>
    )
}