import SubComponent from "./SubComponent";
import SubComponentWithUseMemo from "./SubComponentWithUseMemo";
import {useMemo, useState} from "react";

export default function () {

    const [count, setCount] = useState(0);
    console.log('MemoPage render');

    // const array = useMemo(()=>{
    //     return [1,2];
    // },[]);

    return (
        <>
            <h1>Memo</h1>
            <h2>Clicked times: {count}</h2>
            <button onClick={() => {
                setCount((count) => {
                    return count + 1
                })
            }}>click
            </button>

            <SubComponent></SubComponent>
            <SubComponent></SubComponent>
            <SubComponentWithUseMemo numbers={[1,2]}></SubComponentWithUseMemo>


            <h1>Use Memo</h1>
        </>
    )
}

