import Loader from './components/Loader';
import Film from './components/Film';

import './App.css';
import {BrowserRouter, Routes, Route, Navigate} from "react-router-dom";
import FilmListPage from "./pages/FilmListPage";
import SingleFilmPage from "./pages/SingleFilmPage";

function App() {

return(
    <BrowserRouter >
        <Routes>
            <Route path='/' element={<Navigate to='/films'/>} />
            <Route path='/films' element={<FilmListPage/>} />
            <Route path='/films/:id' element={<SingleFilmPage/>} />
        </Routes>
    </BrowserRouter>
)

}

export default App;
