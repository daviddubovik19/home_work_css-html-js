import {Outlet, NavLink} from "react-router-dom"

const Layout = () => {
    return (
        <>
            <header>
                <nav className="navbar navbar-expand-lg bg-light container">
                    <div className="container-fluid">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <NavLink className={({isActive}) => isActive ? 'nav-link active-nav' : 'nav-link'}
                                             to="/">Home</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/about-us">About us</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/contacts">Contacts</NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>

            <div className="container">
                <Outlet/>
            </div>
        </>
    )
}

export default Layout;