import {BrowserRouter, Routes, Route, useRoutes} from "react-router-dom";

import Layout from "./pages/Layout";
import HomePage from "./pages/HomePage";
import AboutUs from "./pages/AboutUs";
import Contacts from "./pages/Contacts";

function App() {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout />}>
                        <Route index element={<HomePage />}></Route>
                        <Route path="about-us" element={<AboutUs />}></Route>
                        <Route path="contacts" element={<Contacts />}></Route>
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;
