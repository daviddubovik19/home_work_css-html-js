import {useRoutes} from "react-router-dom";

import Layout from "../pages/Layout";
import HomePage from "../pages/HomePage";
import AboutUs from "../pages/AboutUs";
import Contacts from "../pages/Contacts";
import PageNotFound from "../pages/PageNotFound";

const Routes = () => {
    return useRoutes([
        {
            path: '/',
            element: <Layout /> ,
            children: [
                {path: '/', element: <HomePage />},
                {path: '/about-us', element: <AboutUs />},
                {path: '/contacts', element: <Contacts />},
                {path: '*', element: <PageNotFound />},
            ]
        }
    ]);
}

export default Routes;