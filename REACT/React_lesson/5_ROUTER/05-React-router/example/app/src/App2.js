import Routes from  './routes'
import {BrowserRouter} from "react-router-dom";

function App2() {
    return (
        <>
            <BrowserRouter>
                <Routes />
            </BrowserRouter>
        </>
    );
}

export default App2;
