export default function Detail({episodeId, openingCrawl}) {
    return <div>
        <p>Episode: {episodeId}</p>
        <p>openingCrawl {openingCrawl}</p>
    </div>
}