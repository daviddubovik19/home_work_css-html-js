import Loader from './components/Loader';
import Film from './components/Film';
import {useState, useEffect} from 'react';
import './App.css';

function App() {

  const [isLoading, setIsLoading] = useState(true);

  const [films, setFilms] = useState([]);

  useEffect(() => {
    fetch('https://ajax.test-danit.com/api/swapi/films')
      .then(res => res.json())
      .then(films => {
        setFilms(films);
        setIsLoading(false);
      })
  }, [])

  if (isLoading) {
    return <Loader />;
  }

  return <ol>{films.map((film) => {
    return <Film key={film.id} name={film.name} episodeId={film.episodeId} openingCrawl={film.openingCrawl}/>
  })}</ol>

}

export default App;
