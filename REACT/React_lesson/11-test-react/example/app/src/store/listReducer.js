const initialState = {
    listItems: [
        'item 1', 'item 2', 'item 3'
    ],
}

export default function reducer(
    state = initialState,
    action
) {
    switch (action.type) {
        case 'LIST_ADD':
            return {...state, listItems: [...state.listItems, action.payload]}
        case 'LIST_RESET':
            return {...state, listItems: []};
        default:
            return state;
    }
}

export const addItemAction = () => {
    const value = document.querySelector('[name="text"]').value;
    return {type: 'LIST_ADD', payload: value};
}