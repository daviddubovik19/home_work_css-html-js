import {createBrowserRouter} from "react-router-dom";

import Layout from "../layouts/Layout";
import BlogLayout from "../layouts/BlogLayout";

import HomePage from "../pages/HomePage";
import AboutUs from "../pages/AboutUs";
import Contacts from "../pages/Contacts";
import PageNotFound from "../pages/PageNotFound";
import PageWithLoader from "../pages/PageWithLoader"
import Redux from "../pages/ReduxPage";
import ReduxThunkPage from "../pages/ReduxThunkPage";
import ReduxThunkWithSlicePage from "../pages/ReduxThunkWithSlicePage";
import UncontrolledFormPage from "../pages/UncontrolledFormPage";
import ControlledFormPage from "../pages/ControlledFormPage";
import FormikPage from "../pages/FormikPage";
import UseReducerPage from "../pages/UseReducerPage";
import ContextPage  from "../pages/ContextPage";
import MemoPage from "../pages/MemoPage"
import UseCallbackPage from "../pages/UseCallbackPage"


const Router = createBrowserRouter(
    [
        {
            path: '/',
            element: <Layout/>,
            children: [
                {path: '/', element: <HomePage/>},
                {path: 'about-us', element: <AboutUs/>},
                {path: 'contacts', element: <Contacts/>},
                {path: 'redux', element: <Redux/>},
                {
                    path: 'page-with-loader/:id',
                    element: <PageWithLoader/>,
                    loader: ({params}) =>
                        fetch('https://umirs.com/content.php?page=' + params.id).then(res => res.json()),
                },
                {path: 'thunk', element: <ReduxThunkPage/>},
                {path: 'thunk-with-slice', element: <ReduxThunkWithSlicePage/>},
                {path: 'uncontrolled-form-page', element: <UncontrolledFormPage/>},
                {path: 'controlled-form-page', element: <ControlledFormPage/>},
                {path: 'formik-page', element: <FormikPage/>},
                {path: 'use-reducer', element: <UseReducerPage/>},
                {path: 'context', element: <ContextPage/>},
                {path: 'memo', element: <MemoPage/>},
                {path: 'use-callback', element: <UseCallbackPage/>},


            ]
        },
        {path: '*', element: <PageNotFound/>},
    ]);


export default Router;