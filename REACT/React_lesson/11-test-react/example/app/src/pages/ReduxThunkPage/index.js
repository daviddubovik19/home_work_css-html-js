import {useSelector, useDispatch} from "react-redux";
import {loadPagesAsync} from '../../store/pageReducer'

export default function () {

    const pages = useSelector(state => state.pages.items);

    const dispatch = useDispatch();

    // async function loadPages() {
    //     const response = await fetch('https://umirs.com/content.php?page=1')
    //     const loadedPages = await response.json();
    //     dispatch({type:'LOAD_PAGES', payload: {items: loadedPages}})
    // }

    //<!--<button onClick={loadPages}>Load pages (own handler)</button>-->

    return (
        <>
            <h1>Redux Thunk Page</h1>

            <button onClick={() => dispatch({type: 'RESET'}) }>Reset pages</button>
            <button onClick={() => dispatch(loadPagesAsync()) }>Load pages (with action)</button>
            <ul>
                {pages.map((page)=>{
                    return <li key={page.content_id}>{page.name}</li>
                })}
            </ul>
        </>
    )
}