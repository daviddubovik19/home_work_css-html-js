import {memo, useMemo} from "react";

function SubComponentWithMemo({numbers}) {

    console.log('SubComponentWithMemo render');
    // let summedValue = expensiveCalculation(numbers);
    const summedValue = useMemo(()=>{
        return expensiveCalculation(numbers);
    }, numbers);

    return (
        <>
            <h3>SubComponentWithMemo</h3>
            {numbers.join('+')} : {summedValue}
        </>
    )
}

const expensiveCalculation = nums => {
    console.log('expensiveCalculation');
    return nums[0] + nums[1];
}

export default memo(SubComponentWithMemo)