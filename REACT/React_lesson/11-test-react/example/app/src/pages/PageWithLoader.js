import {useLoaderData} from 'react-router-dom';

export default function PageWithLoader() {

    const data = useLoaderData();

    return (
        <>
            <h1>Page With Loader</h1>
            <ul>
                {data.map((post)=>{
                    return <li key={post.content_id}>{post.name}</li>
                })}
            </ul>
        </>
    )
}