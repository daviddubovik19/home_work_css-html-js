import {useState} from "react";
import Subcomponent from "./Subcomponent";

const Component = () => {
    const [count, setCount] = useState(0);

    const handlerCounter = () => {
        setCount(count => count + 1);
    }

    return (
        <>
        <p>test</p>
            <Subcomponent count={count} handlerCounter={handlerCounter}></Subcomponent>
        </>
    )
}

export default Component;