import Component from "./index";
import {fireEvent, render, screen} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe('Component testing', () => {
    test('Component should increment', () => {

        const view = render(<Component />);
        const button = screen.getByText('IncrementButton');
        const counter = screen.getByRole('counter');

        expect(counter.textContent).toBe('clicked 0');

        fireEvent.click(button);

        expect(counter.textContent).toBe('clicked 1');

        userEvent.click(button)

        expect(counter.textContent).toBe('clicked 2');
    })
})