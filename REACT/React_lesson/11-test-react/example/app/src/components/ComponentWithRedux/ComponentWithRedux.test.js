import ComponentWithRedux from './index';
import {fireEvent, render, screen} from "@testing-library/react";

import {useSelector} from "react-redux";

jest.mock("react-redux");

describe('Component with Redux testing', () => {
    test('Component should increment', () => {
//
        useSelector.mockReturnValue([])

        const view = render(<ComponentWithRedux />);
        const button = screen.getByText('IncrementButton');
        const counter = screen.getByRole('counter');

        expect(counter.textContent).toBe('clicked 0');

        fireEvent.click(button);

        expect(counter.textContent).toBe('clicked 1');
    })
})