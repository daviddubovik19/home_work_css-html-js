export default function (props) {
    const {field, form, label, ...rest} = props;

    console.log(field);
    console.log(form);
    const {name} = field;

    return (
        <>
            <div>
                <label>
                    <input{...field} {...rest} />
                </label>
                {form.errors[name] && form.touched[name] && <div  className='alert alert-danger'>{form.errors[name]}</div>}
            </div>
        </>
    )
}