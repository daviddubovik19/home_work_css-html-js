
import {Field, Form, Formik} from "formik";
import MyInput from "./MyInput";

import * as Yup from 'yup';

export default function () {


    const handleSubmit = (values,  {setSubmitting}) => {
        console.log(values);

        setTimeout(() => {
            setSubmitting(false);
        }, 3000);
    }


    return (<>
        <h1>Formik Page</h1>
        <Formik initialValues={{
            email: '',
            password: '',
        }}

                onSubmit={handleSubmit}
                validationSchema={Yup.object({
                    email: Yup.string().email('Invalid email').max(20, 'Must be 20 characters or less').required('Email is required'),
                    password: Yup.string().max(20, 'Must be 20 characters or less').required('Password is required'),
                })}

        >
            <Form noValidate>

                        <Field
                            label="Email"
                            type="email"
                            placeholder="Email"
                            name="email"
                            component={MyInput}
                        ></Field>



                        <Field
                            label="Password"
                            type="password"
                            placeholder="password"
                            name="password"
                            component={MyInput}
                        />

                <div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </div>
            </Form>
        </Formik>
    </>);
}