import {useRef, useState} from "react";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

export default function (){

    const [isSubmitting, setIsSubmitting] = useState(false);
    const [errors, setErrors] = useState({});

    const emailRef = useRef();
    const passwordRef = useRef();

    const validateForm = () => {
        const newErrors = {};
        const email = emailRef.current.value;
        const password = passwordRef.current.value;

        if(!email) {
            newErrors.email = 'Email is required';
        } else if(!EMAIL_REGEX.test(email)) {
            newErrors.email = 'Email is not valid';
        }

        if(!password) {
            newErrors.password = 'Password is required';
        }

        return newErrors;
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        setIsSubmitting(true);

        const newError = validateForm();
        setErrors(newError);

        if(Object.keys(newError).length) {
            setIsSubmitting(false);
            return;
        }


        setTimeout(() => {
            const email = emailRef.current.value;
            const password = passwordRef.current.value;

            console.log(email, password);
            setIsSubmitting(false);
        }, 3000);
    }

    return (<>
    <h1>Uncontrolled form Page</h1>
        <form onSubmit={handleSubmit} noValidate>
            <div>
                <label>
                    <input type="email" name="email" placeholder="Email"  ref={emailRef} defaultValue='' />
                </label>
                {errors.email && <div className='alert alert-danger'>{errors.email}</div>}
            </div>
            <div>
                <label>
                    <input type="password" name="password" placeholder="Password"  ref={passwordRef} />
                </label>
                {errors.password && <div className='alert alert-danger'>{errors.password}</div>}
            </div>

            <div>
                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
            </div>

        </form>
    </>);
}