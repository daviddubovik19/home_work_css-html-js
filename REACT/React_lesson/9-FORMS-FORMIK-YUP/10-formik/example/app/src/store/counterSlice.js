import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";

const initialState = {
    clicked: 0,
    status: '',
}

export const fetchAsync = createAsyncThunk(
    'counter/fetchAsync',
    async (_, {rejectWithValue}) => {
        try {
            let response = await fetch('https://umirs.com/content.php?get_count=1');
            let result = await response.json();
            return result;
        } catch (error) {
            return rejectWithValue(error.message);
        }
    }
)

const counterSlice = createSlice({
    name: 'counter',
    initialState: initialState,
    reducers: {
        increment(state, action) {
            console.log(action);
            state.clicked += action.payload.step;
        },
        decrement(state, action) {
            state.clicked -= 1;
        },
        reset(state) {
            state.clicked = 0;
        }
    },
    extraReducers: {
        [fetchAsync.pending]: (state, action) => {
            state.status = 'loading';
        },
        [fetchAsync.fulfilled]: (state, action) => {
            state.status = 'loaded';
            state.clicked = action.payload.count;
        },
        [fetchAsync.rejected]: (state, action) => {
            state.status = 'rejected: error ' + action.payload;
            state.clicked = 0;
        }
    }
})

export const {increment, decrement, reset} = counterSlice.actions;

export default counterSlice.reducer;