import React from 'react';
import {Outlet} from "react-router-dom";
import Header from "../components/Header";

const Layout = ({user, setUser}) => {
    return (
        <div>
            <Header user={user}  setUser={setUser}/>
            <main>
                <Outlet/>
            </main>
        </div>
    );
};

export default Layout;