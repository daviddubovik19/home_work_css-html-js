import {Navigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {useFormik} from "formik"
import * as Yup from "yup"

const initialValues = {
    login: '',
    password: ''
}

const validationSchema = Yup.object({
    login: Yup.string()
        .required("Login is required")
        .min(4, "field should be greater or equal than 4")
        .max(8, "field should be less or equal than 8"),
    password: Yup.string()
        .required('Введите пароль')
        .matches(/^(?=.*[A-Z]).+$/, 'Пароль должен содержать хотя бы одну большую букву'),

})


export default function () {


    const dispatch = useDispatch()
    const user = useSelector(state => {
        return state.login.user
    })

    const formik = useFormik({
        initialValues,
        validationSchema,
        onSubmit: values => {

            localStorage.setItem('login', values.login)
            localStorage.setItem('password', values.password)

            dispatch({type: "LOGIN", payload: {user: values.login}})

        }
    })

    return user ? <Navigate to='/films'/> :
        (
            <form onSubmit={formik.handleSubmit}>
                <input
                    className="p-2 m-2"
                    type="text"
                    name="login"
                    value={formik.values.login}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />
                {formik.errors.login && formik.touched.login ? <p>{formik.errors.login}</p> : null}

                <input
                    className="p-2 m-2"
                    type="text"
                    name="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                />
                {formik.errors.password && formik.touched.password ? <p>{formik.errors.password}</p> : null}

                <button className="btn-primary btn" type='submit' disabled={formik.isSubmitting}>отправить</button>
            </form>
        )
}
