import {Navigate} from "react-router-dom";
import {useSelector} from "react-redux";

export default function ({element}){
    const user = useSelector(state => {
        return state.login.user
    })
    return  user ? element : <Navigate to='/login'/>



}




