import Detail from "../../components/Detail";
import {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
export default function SingleFilmPage (){

    const [film, setFilm] = useState({});
    const {id} = useParams()

    useEffect(() => {
        fetch('https://ajax.test-danit.com/api/swapi/films/'+ id)
            .then(res => res.json())
            .then(film => {
                setFilm(film);
            })
    }, [])

    return !film.id
        ? <div>Loading...</div>
        : (<Detail episodeId={film.episodeId}
                   openingCrawl={film.openingCrawl}>
            </Detail>
            )
}