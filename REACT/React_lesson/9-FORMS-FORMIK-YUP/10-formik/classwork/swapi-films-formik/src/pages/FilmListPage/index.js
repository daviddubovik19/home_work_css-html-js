import Loader from "../../components/Loader";
import Film from "../../components/Film";
import {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

export default function FilmListPage (){
    const [isLoading, setIsLoading] = useState(true);

    const films = useSelector(state => {
        return state.films.list
    })
     const dispatch = useDispatch()

    useEffect(() => {
        fetch('https://ajax.test-danit.com/api/swapi/films')
            .then(res => res.json())
            .then(films => {
                dispatch({type:'ITEMS', payload: {films: films}});
                setIsLoading(false);
            })
    }, [])


    if (isLoading) {
        return <Loader />;
    }

    return (

        <ol>
            {films.map((film) => {
                return <Film key={film.id} name={film.name} episodeId={film.episodeId} openingCrawl={film.openingCrawl}/>
            })}
        </ol>

    )
}
