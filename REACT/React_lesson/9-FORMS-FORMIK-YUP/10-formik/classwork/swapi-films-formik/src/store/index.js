import {configureStore} from "@reduxjs/toolkit";
import filmReducer from "./filmReducer";
import thunk from "redux-thunk";
import userReducer from "./userReducer";

const store = configureStore({
    reducer: {films: filmReducer,
               login: userReducer},
    middleware: [thunk]
})

export default store;

