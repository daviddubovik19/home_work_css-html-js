
const initialStore = {
    list: []

}

export default function filmReducer(state=initialStore, action){
    switch (action.type){
        case "ITEMS":
            return {...state, list: action.payload.films}
        default:
        return state
    }
}

