
const initialStore = {
    user: null
}
export default function userReducer(state=initialStore, action){
    switch (action.type){
        case "LOGIN":
            return {...state, user: action.payload.user}
        case "LOGOUT":
            return {...state, user: null}
        default:
            return state
    }
}