import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import FilmListPage from "../pages/FilmListPage";
import ProtectedPage from "../pages/ProtectedPage";
import SingleFilmPage from "../pages/SingleFilmPage";
import LoginPage from "../pages/LoginPage";

import Layout from "../layouts/Layout";

export default function ({user, setUser}){

    return(
        <BrowserRouter >
            <Routes>
               <Route path='/'  element={<Layout  user={user} setUser={setUser}/>}>
                   <Route index element={<Navigate to='/films'/>} />
                   <Route path='/films' element={<FilmListPage/>} />
                   <Route path='/films/:id' element={<ProtectedPage user={user} element={<SingleFilmPage/>}/>} />
                   <Route path='/login' element={<LoginPage user={user} setUser={setUser}/>} />
               </Route>
            </Routes>
        </BrowserRouter>
    )
}