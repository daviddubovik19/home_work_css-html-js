import {Formik, Form, Field, ErrorMessage} from 'formik';
import React from 'react';
// import {useFormik} from 'formik';
import * as Yup from 'yup';

const schema = Yup.object().shape({
    name: Yup.string().min(3,'name error').max(8,'max string 8').required('Required'),
    email:Yup.string().email('email error').required('Required 2')
});
// У цьому прикладі object() створює об'єкт, що містить перевірки для кожної властивості.
// string() створює перевірку для рядкового значення, а min(3) вимагає, щоб рядок був не менше 3 символів.
// required() вимагає, щоб поле було заповненим.


// const data = { name: 'John' };


const App = () => {
    return (
        <Formik
            initialValues={{name: '', email: ''}}
            onSubmit={(values) => {
                schema.validate(values)
                    .then(function (values) {
                        console.log(values);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }}
        >
            <Form>
                <Field type="text" name="name"/>
                {/*<ErrorMessage name="name"/>*/}

                <Field type="email" name="email"/>
                {/*<ErrorMessage name="email"/>*/}
                <button type="submit">Submit</button>

            </Form>
        </Formik>
    )
}

// <========================================================>

// const App = () => {
//     const formik = useFormik({ // це спеціальний хук React, який повертає весь стан Formik і хелпери безпосередньо. Незважаючи на свою назву, він не призначений для більшості випадків використання.
//         initialValues: {
//             email: '',
//         },
//         onSubmit: values => {
//             alert(JSON.stringify(values, null, 2));
//         },
//     });
//     return (
//         <form onSubmit={formik.handleSubmit}>
//             <label htmlFor="email">Email Address</label>
//             <input
//                 id="email"
//                 name="email"
//                 type="email"
//                 onChange={formik.handleChange}
//                 value={formik.values.email}
//             />
//
//             <button type="submit">Submit</button>
//         </form>
//
//     );
// };
// <==========================================================>


// const App = () => {
//
//     const formik = useFormik({
//         initialValues: {
//             firstName: '',
//             lastName: '',
//             email: '',
//         },
//         onSubmit: values => {
//             console.log(values, null, 2)
//         },
//     });
//     return (
//         <form onSubmit={formik.handleSubmit}>
//             <label htmlFor="firstName">First Name</label>
//             <input
//                 id="firstName"
//                 name="firstName"
//                 type="text"
//                 onChange={formik.handleChange}
//                 value={formik.values.firstName}
//             />
//
//             <label htmlFor="lastName">Last Name</label>
//             <input
//                 id="lastName"
//                 name="lastName"
//                 type="text"
//                 onChange={formik.handleChange}
//                 value={formik.values.lastName}
//             />
//
//             <label htmlFor="email">Email Address</label>
//             <input
//                 id="email"
//                 name="email"
//                 type="email"
//                 onChange={formik.handleChange}
//                 value={formik.values.email}
//             />
//
//             <button type="submit">Submit</button>
//         </form>
//     );
// };
// <==========================================================>


export default App;
