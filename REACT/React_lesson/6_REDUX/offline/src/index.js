import React from 'react';
import ReactDOM from 'react-dom/client';

import {Provider} from "react-redux";
import {createStore} from "redux"
import allReducers from "./reducers";
import WebPage from "./componens/WebPage";

const store = createStore(allReducers)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <WebPage/>
        </Provider>

    </React.StrictMode>
);

// function playList(state=[]) {
//     return state
// }
//
// // console.log(store.getState()) // виводить состояние нашено store
// store.subscribe(()=>{ // подписуемя на изменения нашего store, щоб знать що у нас изменились дание в ньом
//     console.log('subscribe', store.getState())
// })
// store.dispatch