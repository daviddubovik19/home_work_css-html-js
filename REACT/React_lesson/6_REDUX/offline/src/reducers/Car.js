export default function () {
    return [
        {
            id:1,
            name:'Audi',
            speed:234.45,
            weight:1.4,
            desc:"R8 це чудовий спортивне авто !",
            img:'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/R8_Coupe_V10_performance-1.jpg/250px-R8_Coupe_V10_performance-1.jpg'
        },
        {
            id:2,
            name:'BWM',
            speed:314.5,
            weight:1.2,
            desc:" авто з позначкой 'M' завжди були швидші ",
            img:'https://thumbs.dreamstime.com/z/bwm-i-serie-%D1%80%D0%BE%D1%81%D0%BA%D0%BE%D1%88%D0%BD%D1%8B%D0%B9-%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9-%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8C-%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8-bmw-%D0%BD%D0%B0-%D0%B0%D0%B2%D1%82%D0%BE%D1%88%D0%BE%D1%83-184916889.jpg'

        },
        { id:3,
            name:'Mercedes',
            speed:286.1,
            weight:1.35,
            desc:"Mercedes відрізняеться не тільки швидкістю, но також комфортом",
            img:'https://cdn3.riastatic.com/photosnew/auto/photo/mercedes-benz__480775836fx.jpg'
        }
    ]
}