// поєднуємо усі бази даних
import {combineReducers} from 'redux'
import CarsReducers from './Car'

const allReducers = combineReducers({
    cars: CarsReducers
})
export default allReducers