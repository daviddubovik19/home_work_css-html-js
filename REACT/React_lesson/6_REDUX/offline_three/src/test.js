import {useSelector, useDispatch} from "react-redux";

const Test = () => {
    const clicked = useSelector(state => { // получает clicked із store
        return state.clicker.clicked
    })
const dispatch =  useDispatch()

    return (
        <>
            <div>
                <h1> clicked: {clicked}</h1>
            </div>
            <div>
                <button onClick={()=>dispatch({type:'INCREMENT', payload:{times: 5} })}>click</button>
            </div>
        </>

    );
};

export default Test;