const initialState = {
    clicked: 1
}
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case 'INCREMENT':
            return {...state, clicked: state.clicked + action.payload.times}
    }
    return {...state}
}