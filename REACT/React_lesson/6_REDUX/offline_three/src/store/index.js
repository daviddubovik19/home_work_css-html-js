import reducer from './reducer'
import {configureStore} from "@reduxjs/toolkit";

const store = configureStore({
    reducer: {clicker: reducer}
})

export default store