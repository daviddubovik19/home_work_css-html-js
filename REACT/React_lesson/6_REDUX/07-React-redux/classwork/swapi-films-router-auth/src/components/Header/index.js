import React from 'react';
import {Link} from "react-router-dom";
import {useSelector,useDispatch} from "react-redux";

const Header = () => {
    const dispatch = useDispatch()
    const user = useSelector(state => {
        return state.login.user
    })
    return (
        <header>
            { user ? <p>welcome, {user} <button onClick={()=> {

                dispatch({type:"LOGOUT"})
            }}>Log Out</button></p> : <Link to='/login'>Login</Link>}
        </header>
    )
};

export default Header;