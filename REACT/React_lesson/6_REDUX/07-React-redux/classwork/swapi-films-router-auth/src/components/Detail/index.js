import {NavLink} from "react-router-dom";


export default function Detail({episodeId, openingCrawl}) {
    return <div>
        <p>Episode: {episodeId}</p>
        <p>OpeningCrawl: {openingCrawl}</p>
        <NavLink to='/films' className='btn btn-secondary'>
            Back
        </NavLink>
    </div>
}