import { useState } from "react";
import Button from "../Button";
import Detail from "../Detail";
import {NavLink} from "react-router-dom";

export default function Film({name, episodeId, openingCrawl}) {

    // const [clicked, setClicked] = useState(false);
    
    return <li>
    <h1>{name}</h1>

        <NavLink to={`/films/${episodeId}`} className='btn btn-primary'>
            Details
        </NavLink>

        {/*{clicked */}
    {/*? */}
    {/*<Detail episodeId={episodeId} openingCrawl={openingCrawl}/>*/}
    {/*: */}
    {/*<Button text='Детальнее' changeClicked={setClicked} />*/}

    {/*}*/}
    </li>
}