import {Navigate} from "react-router-dom";
import {useDispatch,useSelector} from "react-redux";

export default function () {
const user = useSelector(state => {
    return state.login.user
})
const dispatch = useDispatch()
    const onSubmitHandler = (event)=>{
        event.preventDefault()
      const login =  event.target.login.value
        if (login){
            dispatch({type: "LOGIN", payload:{user:login}} )
        }

    }
    return user ? <Navigate to='/films'/> :
        (
            <form onSubmit={onSubmitHandler}>
                <input type="text" name="login"/>
                <button type='submit'>отправить</button>
            </form>
        )
}