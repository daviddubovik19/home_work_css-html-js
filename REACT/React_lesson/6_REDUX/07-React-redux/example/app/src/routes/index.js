import {createBrowserRouter} from "react-router-dom";

import Layout from "../layouts/Layout";
import BlogLayout from "../layouts/BlogLayout";

import HomePage from "../pages/HomePage";
import AboutUs from "../pages/AboutUs";
import Contacts from "../pages/Contacts";
import PageNotFound from "../pages/PageNotFound";
import PageWithLoader from "../pages/PageWithLoader"
import Redux from "../pages/Redux";

const Router = createBrowserRouter(
    [
        {
            path: '/',
            element: <Layout/>,
            children: [
                {path: '/', element: <HomePage/>},
                {path: 'about-us', element: <AboutUs/>},
                {path: 'contacts', element: <Contacts/>},
                {path: 'redux', element: <Redux/>},
                {
                    path: 'page-with-loader/:id',
                    element: <PageWithLoader/>,
                    loader: ({params}) =>
                        fetch('https://umirs.com/content.php?page=' + params.id).then(res => res.json()),
                },
            ]
        },
        {path: '*', element: <PageNotFound/>},
    ]);


export default Router;