import {useSelector, useDispatch} from "react-redux";

import {addItemAction} from '../../store/listReducer'

import {increment, decrement, reset} from "../../store/counterSlice";

const Redux = () => {

    const clicked = useSelector(state => {
        return state.clicker.clicked;
    });

    const list = useSelector(state => {
        return state.list.listItems;
    })

    const counter = useSelector(state => {
        return state.counter.clicked;
    });

    const dispatch = useDispatch();

    return (
        <div>
            <h1>Redux Example</h1>
            <div>
                Clicked times: {clicked}
            </div>
            <div>
                <button onClick={() => dispatch({type: 'INCREMENT'})}>Increment 1 time</button>
            </div>
            <div>
                <button onClick={() => dispatch({type: 'DECREMENT'})}>Decrement 1 time</button>
            </div>
            <div>
                <button onClick={() => dispatch({type: 'INCREMENT', payload: {times: 5}})}>Increment 5 times</button>
            </div>
            <div>
                <button onClick={() => dispatch({type: 'RESET'})}>RESET</button>
            </div>


            <ul>
                {list.map((item) => {
                    return <li>{item}</li>
                })}
            </ul>

            <input name='text'/>
            <button onClick={() => {
                dispatch(addItemAction())
            }}
            >Add
            </button>

            <div>
                <button onClick={() => dispatch({type: 'LIST_RESET'})}>RESET LIST</button>
            </div>
            <hr />
            <h2>Counter Slice: {counter}</h2>
            <button onClick={() => dispatch(increment({step: 3}))}>increment</button>
            <button onClick={() => dispatch(decrement())}>decrement</button>
            <button onClick={() => dispatch({type: 'counter/increment'})}>increment</button>
            <button onClick={() => dispatch(reset())}>reset</button>
        </div>
    );
};

export default Redux;