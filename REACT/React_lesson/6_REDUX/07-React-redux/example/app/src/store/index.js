import {combineReducers, configureStore} from "@reduxjs/toolkit";
import storage from 'redux-persist/lib/storage'
import {persistReducer, persistStore} from "redux-persist";

import clicker from "./clicker";
import counter from "./counterSlice"
import listReducer from "./listReducer";

const persistConfig = {
    key: 'root',
    storage
}

const allReducers = combineReducers({
    clicker: clicker,
    list: listReducer,
    'counter': counter,
});

const persistedReducer = persistReducer(persistConfig, allReducers);

const store = configureStore({
    reducer: persistedReducer
});

export const persistor = persistStore(store);

export default store;