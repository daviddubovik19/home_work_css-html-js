import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    clicked: 12
}

const counterSlice = createSlice({
    name: 'counter',
    initialState: initialState,
    reducers: {
        increment(state, action) {
            console.log(action);
            state.clicked += action.payload.step;
        },
        decrement(state, action) {
            state.clicked -= 1;
        },
        reset(state) {
            state.clicked = 0;
        }
    }
})

export const {increment, decrement, reset} = counterSlice.actions;

export default counterSlice.reducer;