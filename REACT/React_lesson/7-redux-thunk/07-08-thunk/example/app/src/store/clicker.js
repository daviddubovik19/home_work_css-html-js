const initialState = {
    clicked: 0,
}

export default function clicker(
    state = initialState,
    action
) {

    const times = action.payload && action.payload.times ? action.payload.times : 1;

    switch (action.type) {
        case 'INCREMENT':
            return {...state, clicked: state.clicked + times}
        case 'DECREMENT':
            return {...state, clicked: state.clicked - times}
        case 'RESET':
            return initialState
        default:
            return state;
    }


}