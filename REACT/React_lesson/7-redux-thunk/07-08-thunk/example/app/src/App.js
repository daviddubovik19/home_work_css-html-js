import {RouterProvider} from "react-router-dom";
import Loader from "./components/Loader";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";

import router from "./routes"
import store, {persistor} from "./store";

function App() {

    return (
        <>
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <RouterProvider router={router} fallbackElement={<Loader/>}/>
                </PersistGate>
            </Provider>
        </>
    );
}

export default App;
