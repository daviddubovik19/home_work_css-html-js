import './App.css';
import data from './data/data.json'
import { React, Component } from 'react';
import Header from './components/Header/Header';
import ProductList from './components/ProductList/ProductList';
import Cart from './components/Cart/Cart';

export default class App extends Component {

state ={cartItems:[]}

addToCart =(quantity, id)=>{
const product = data.products.filter(item => item.id === id)[0]
product.quantity = quantity
this.setState((prevState)=>({
  cartItems: [...prevState.cartItems, product]
}))
}


  render() {
    return (
      <>
        <Header title='Rozetka' />
        <div className='body'>
          <div className='left-body'>
            <ProductList onProductAdd={this.addToVart} data={data.products}/>
          </div>
          <div className='right-body'><Cart/></div>
        </div>
      </>

    )
  }
}


