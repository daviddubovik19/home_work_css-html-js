import ProductCard from '../ProductCard/ProductCard'


function ProductList(props) {
	console.log(props);
	return (
		<>
			{props.data.map(product => {
				return <ProductCard onQuantityAdd={(quantity) => props.onProductAdd(quantity, product.id)}data={product} key={product.id} />
				
			})}
		</>
	)
}

export default ProductList;