import { Component } from 'react';



export default class ProductCard extends Component {
	constructor(props) {
		super(props);
		this.state = { value: "" }
	}

	onClickHandler = (e) => {
		this.setState({
			value: e.target.value
		})
	}
	onItemAdd = () => {
		this.props.onProductAdd(this.state.value)
		this.setState({
			value: ""
		})
	}

	render() {
		return (
			<div className='product-card'>
				<h4>{this.props.data.name}</h4>
				<img src={this.props.data.img_url} />
				<p>{this.props.data.price}</p>

				<form>
					<input type='number' onChange={this.onChangeInput} value={this.state.value} />
					<button type='button' onChange={this.onClickHandler}>Add to Cart</button>
				</form>
			</div>

		)

	}
}