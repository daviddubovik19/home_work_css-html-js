import React,{useState} from "react";
import City from "./component/City";
import CitiesList from "./component/CitiesList";
// Поднятие состояния, якщо в нас є якісь дані то треба йх передати в ближайший родительский елемент і потім опуситить ці дані в функції
function App() {
    const [cities, setCities] = useState([
        {name: 'Kiev', description: 'Столиця України'},
        {name: 'Lviv', description: 'cool'}
    ])
    const [currentIndex,setCurrentIndex] =useState(0)
    const handlerChangeCity=(n,description)=>{
        setCities(cities.map((city, index) => {
            if (index === n){
                return {
                    ...city,
                    description,
                }
            }
            return city
        }))
    }
    const handlerSelectCity =(n)=>{setCurrentIndex(n)}
  return (
   <div>
       <City cities={cities} onChangeCity={handlerChangeCity} currentIndex={currentIndex}/>
     <CitiesList cities={cities} onSelectCity={handlerSelectCity}/>
   </div>
  );
}

export default App;
