const initialState = {
    items: [],
}

export default function pages(
    state = initialState,
    action
) {
    switch (action.type) {
        case 'LOAD_PAGES':
            return {...state, items: action.payload.items}
        case 'RESET':
            return {...state, items: []}
        default:
            return state;
    }
}

export const loadPagesAsync = () => {
    return async (dispatch) => {
        const response = await fetch('https://umirs.com/content.php?page=1')
        const pages = await response.json();

        dispatch({type: 'LOAD_PAGES', payload: {items: pages}})
    }
}


///What thunk does
// function dispatch(someFunction)//Fake Dispatch
// {
//     const dispatch = useDispatch();//Real Dispatch
//     if(typeof someFunction === 'object') {
//         dispatch(someFunction)
//     } else {
//         someFunction(dispatch);
//     }
// }