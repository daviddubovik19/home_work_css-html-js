import {useState, useEffect} from "react";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

export default function () {

    const [isSubmitting, setIsSubmitting] = useState(false);
    const [errors, setErrors] = useState({});
    const [touched, setTouched] = useState({});
    const [values, setValues] = useState({
        email: '',
        password: '',
    });

    const validateForm = () => {
        const newErrors = {};

        const {email, password} = values;

        if (!email) {
            newErrors.email = 'Email is required';
        } else if (!EMAIL_REGEX.test(email)) {
            newErrors.email = 'Email is not valid';
        }

        if (!password) {
            newErrors.password = 'Password is required';
        }

        return newErrors;
    }

    const handleChange = (e) => {
        const {name, value} = e.target;
        setValues({...values, [name]: value});
    }

    const handleBlur = (e) => {
        const {name} = e.target;
        setTouched({...touched, [name]: true})
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setIsSubmitting(true);

        setTouched({email: true, password: true})

        const newError = validateForm();
        setErrors(newError);

        if (Object.keys(newError).length) {
            setIsSubmitting(false);
            return;
        }

        setTimeout(() => {
            const {email, password} = values;
            console.log(email, password);
            setIsSubmitting(false);
        }, 3000);
    }

    useEffect(() => {
        setErrors(validateForm())
    }, [values])

    return (<>
        <h1>Controlled form Page</h1>
        <form onSubmit={handleSubmit} noValidate>
            <div>
                <label>
                    <input
                        type="email"
                        name="email"
                        placeholder="Email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </label>
                {errors.email && touched.email && <div className='alert alert-danger'>{errors.email}</div>}
            </div>
            <div>
                <label>
                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                    />
                </label>
                {errors.password && touched.password && <div className='alert alert-danger'>{errors.password}</div>}
            </div>

            <div>
                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
            </div>

        </form>
    </>);
}