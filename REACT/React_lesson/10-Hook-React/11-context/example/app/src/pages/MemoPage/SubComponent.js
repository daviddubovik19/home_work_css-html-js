import {memo} from "react";

function SubComponent() {

    console.log('Subcomponent render');

    return (
        <>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus atque cupiditate distinctio ducimus magni non quis ratione recusandae. Magni optio quisquam sint sit. Ad commodi consequatur deserunt eveniet, illum in!</p>
        </>
    )
}

export default memo(SubComponent)