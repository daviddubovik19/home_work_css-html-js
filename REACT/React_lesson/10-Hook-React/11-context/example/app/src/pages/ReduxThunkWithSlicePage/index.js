import {useSelector, useDispatch} from "react-redux";
import {decrement, increment, reset, fetchAsync} from "../../store/counterSlice";

export default function () {

    const counter = useSelector(state => {
        return state.counter.clicked;
    });

    const status = useSelector(state => {
        return state.counter.status;
    });

    const dispatch = useDispatch();


    return (
        <div>
            <h1>Redux Thunk with Slice Example</h1>
            <hr />
            <h2>Counter Slice: {counter}</h2>
            <h2>Status: {status}</h2>
            <button onClick={() => dispatch(increment({step: 1}))}>increment 1</button>
            <button onClick={() => dispatch(increment({step: 3}))}>increment 3</button>
            <button onClick={() => dispatch(decrement())}>decrement</button>
            <button onClick={() => dispatch(reset())}>reset</button>
            <hr />
            <button onClick={() => dispatch(fetchAsync())}>fetchAsync</button>
        </div>
    );
}