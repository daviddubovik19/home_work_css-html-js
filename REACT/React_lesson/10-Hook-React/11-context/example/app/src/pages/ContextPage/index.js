import ThemeContext from "../../context";
import {useContext} from "react";

export default function () {

    const {theme} = useContext(ThemeContext);

    return (
        <>
            <h1>Context Example</h1>
            <p style={{color: theme.color}}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis doloribus eaque eos illum incidunt ipsa itaque libero molestiae, possimus qui ratione reiciendis soluta suscipit tenetur vitae voluptate voluptates.</p>

        </>
    )
}