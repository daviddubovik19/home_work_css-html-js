import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './component/App';
// const  element = <h1>hello, React</h1>


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App/>);

