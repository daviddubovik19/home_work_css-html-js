const Balance = ({ balance }) => {   // children це зарезервоване слова що відповідає за контент який зберигаеться в межах тегу Balance
    return (
        <div>
            {/* {!balance &&  'повний нуль'} */}
            {/* {!balance ===  0 ? 'повний нуль' : balance < 0 ? 'зайшли в мінус' : 'ми в плюсі'} */}
            
            {balance}
        </div>
    )
}
export default Balance


// const Balance = ({ balance, children }) => {   // children це зарезервоване слова що відповідає за контент який зберигаеться в межах тегу Balance
//     return (
//         <div>
// {children}
//         {balance}
//         </div>
//     )
// }

//  {/* <Balance balance={this.state.balance}>Мої заощадження</Balance> */}