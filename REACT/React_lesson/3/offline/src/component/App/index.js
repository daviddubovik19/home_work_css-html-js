
import React, { Component } from "react"
import Balance from "../Balance"
import Transactions from "../Transactions";
let id = 0;
class App extends Component {
    constructor() {
        super()
        this.state = {
            balance: 0,
            transactions: []
        }
        this.onIncrease = this.onIncrease.bind(this)
        console.log('constructor');
    }
    // componentDidMount(){
    //     const balance = JSON.parse(window.localStorage.get('balance'));
    //     this.setState({
    //         balance
    //     })
    //     console.log('componentDidMount');
    // }
    // componentWillUnmount(){
    //     window.localStorage.setItem('balance',JSON.stringify(this.state.balance))
    //     console.log('componentWillUnmount');
    // }
    // shouldComponentUpdate(nextProps,nextState){
    //     console.log('shouldComponentUpdate');
    //     return nextState.balance < 5;
    // }
    onIncrease() {
        this.setState((state) => ({
            balance: state.balance + 1,
            transactions: [{
                label: "increase",
                value: 1,
                id: ++id
            }, ...state.transactions]
        }))
    }
    onDecrease = () => {
        this.setState((state) => ({
            balance: state.balance - 1,
            transactions: [{
                label: "decrease",
                value: -1,
                id: ++id
            }, ...state.transactions]
        }))
    }
    render() {

        return (
            <div>
                <Balance balance={this.state.balance} />
                <button onClick={this.onIncrease}>Додати 1</button>
                <button onClick={this.onDecrease}>Відняти 1</button>
                <hr />
                {/* {this.state.transactions.length === 0  ? "ще немає транзакцій": JSON.stringify(this.state.transactions)}   */}
              <Transactions transactions={this.state.transactions}/>
            </div>

        )
    }
}
export default App