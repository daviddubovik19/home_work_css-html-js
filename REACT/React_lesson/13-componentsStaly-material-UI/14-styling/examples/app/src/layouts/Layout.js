import {Outlet, NavLink} from "react-router-dom"
import ThemeContext from "../context";
import {useContext} from "react";

const Layout = () => {

    const {setColor} = useContext(ThemeContext);

    return (
        <>
            <button onClick={() => {
                setColor({color: 'yellow'})
            }}>set yellow
            </button>
            <button onClick={() => {
                setColor({color: 'black'})
            }}>set black
            </button>
            <header>
                <nav className="navbar navbar-expand-lg bg-light container">
                    <div className="container-fluid">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <NavLink className={({isActive}) => isActive ? 'nav-link active-nav' : 'nav-link'}
                                             to="/">Home</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/about-us">About us</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/contacts">Contacts</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/page-with-loader/1">PageWithLoader</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/redux">Redux</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/thunk">Thunk</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/thunk-with-slice">Thunk with Slice</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/uncontrolled-form-page">Uncontrolled
                                        form</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/controlled-form-page">Controlled form</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/formik-page">Formik</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/use-reducer">Use Reducer</NavLink>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>
                <nav className="navbar navbar-expand-lg bg-light container">
                    <div className="container-fluid">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/context">Context</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/memo">Memo</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/use-callback">useCallback</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/page-with-redux">redux Test</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/css-modules">css Modules</NavLink>
                                </li>

                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/styled">styled</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/material">material</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className='nav-link' to="/bootstrap">bootstrap</NavLink>
                                </li>


                            </ul>
                        </div>
                    </div>
                </nav>
            </header>

            <div className="container">
                <Outlet/>
            </div>
        </>
    )
}

export default Layout;