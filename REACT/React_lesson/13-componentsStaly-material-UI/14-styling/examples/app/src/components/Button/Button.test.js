import ReactDOM from "react-dom/client";
import Button from "./index";

import {act} from 'react-dom/test-utils';

global.IS_REACT_ACT_ENVIROMENT = true;

let container = null;
let root = null;

beforeEach(() => {
    container = document.createElement('div');
    document.body.append(container);
    root = ReactDOM.createRoot(container);
});

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});


test("Button test", () => {
    act(() => {
        root.render(<Button title='React' />);
    });

    const element = document.querySelector('button');
    expect(element).not.toBeUndefined();
    expect(element).toBeTruthy();

    expect(container.textContent).toBe('Hi, React');
    expect(element.textContent).toBe('Hi, React');

    expect(element.outerHTML).toMatchSnapshot();
});
