import {useDispatch, useSelector} from "react-redux";

export default function() {
    const status = useSelector(state => state.clicker.clicked);

    const dispatch = useDispatch();

    return (
        <>
            <p role='status'>status from Redux: {status}</p>
            <button role='button' onClick={()=>{ dispatch({type: 'INCREMENT' }) }}>Click</button>
        </>
    )
}