import styled, {css} from 'styled-components';

const Title = styled.h1`
    color: red;
    ${props => 
        props.primary && css`
        background: green;
        border: 1px solid red;
        `}
`;

const SuperTitle = styled(Title)`
    color: gold;
`;

const Index = () => {
    return (
        <div>
            <h1>Styled</h1>
            <Title primary>Styled Title</Title>
            <SuperTitle primary>Super Title</SuperTitle>
        </div>
    );
};

export default Index;