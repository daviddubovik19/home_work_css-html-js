import {useReducer, useState} from "react";

export default function () {

    //const [showAlert, setShowAlert] = useState(false);

    const showAlertReducer = (state, action) => {
        switch (action.type) {
            case "SHOW":
                return {...state, show: true};
            case "HIDE":
                return {...state, show: false};
            default:
                return state;
        }
    }

    const [showAlert, showAlertDispatcher] = useReducer(showAlertReducer, {show: false});

    return (
        <>
        <h1>useReducer Example page</h1>

            {showAlert.show && <div className="alert alert-primary" role="alert">
                This is a primary alert—check it out!
            </div>}

            <button onClick={()=>{ showAlertDispatcher({type: "SHOW"}) }} className='show'>Show Alert!</button>
            <button onClick={()=>{ showAlertDispatcher({type: "HIDE"}) }} className='hide'>Hide Alert!</button>
        </>
    )
}