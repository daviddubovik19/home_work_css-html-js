import styles from './CSSModulesPage.module.scss'
import classNames from 'classnames';

export default function () {

    const selected = true;

    return (
        <>
            <h1>Css Modules Page</h1>
            <p className={styles['css-modules']}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut,
                consequuntur dolorem ducimus expedita ipsum libero neque quos repudiandae tenetur? Atque est expedita
                ipsum iusto maxime odio quia quo quos?</p>
            <p className={styles.textBlue}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda
                debitis eius eligendi eos est exercitationem incidunt maxime molestias mollitia necessitatibus nesciunt
                quasi quidem, repellat sequi sint temporibus veniam vero.</p>
            <p className='text-red'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam assumenda
                dolores eum minima sit veritatis? Asperiores eos nobis sed. Aspernatur consectetur dignissimos dolorum
                et mollitia optio quas quibusdam repellendus.</p>

            <p className={classNames(styles['text-blue'], {
                [styles.bold]: true,
                [styles['selected']]: selected,
            })}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias beatae blanditiis corporis cumque delectus dicta eaque eveniet fuga, ipsum minus non quae quos, rem saepe sint temporibus ullam, veritatis voluptates.</p>

        </>
    )
}