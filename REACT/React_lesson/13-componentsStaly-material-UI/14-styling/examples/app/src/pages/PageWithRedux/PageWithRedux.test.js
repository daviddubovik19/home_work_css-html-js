import {fireEvent, render, screen} from "@testing-library/react";
import PageWithRedux from "./index";
import {useDispatch, useSelector} from "react-redux";

jest.mock("react-redux", () => ({
    ...jest.requireActual('react-redux'),
    useSelector: jest.fn(),
    useDispatch: jest.fn()
}));
//
// jest.mock("react-redux");

const dispatch = jest.fn();

describe('Page with Redux testing', () => {
    test('Component should increment', () => {

        useDispatch.mockImplementation(()=>{
            return dispatch;
        });

        useSelector.mockImplementation(callback => {
            return callback({clicker: {clicked: 5}})
        })

        const view = render(<PageWithRedux/>);

        const button = screen.getByRole('button');
        const status = screen.getByRole('status');
        expect(button).toBeTruthy();
        expect(status).toBeTruthy();

        expect(status.textContent).toBe('status from Redux: 5');

        expect(dispatch).toHaveBeenCalledTimes(0);
        fireEvent.click(button);

        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(dispatch).toHaveBeenCalledWith({type: 'INCREMENT' });

    })
})