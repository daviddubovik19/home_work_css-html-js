import React from 'react';
import ControlledAccordions from '../../components/ControlledAccordions'
import {Button} from "@mui/material";
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';


const Index = () => {
    return (
        <div>
            <h1>Material page</h1>
            <ControlledAccordions square={true}></ControlledAccordions>
            <br />
            <Button variant="contained"> <AddShoppingCartIcon />Add to cart</Button>
        </div>
    );
};

export default Index;