const sum = (a, b) => +a + +b;

describe('Testing sum function', () => {
    test('1 + 1 should be equal 2', () => {
        const resultOfSum = sum(1, 1);
        expect(resultOfSum).toBe(2);
    })

    it('1 + "b" should be equal NaN', () => {
        const resultOfSum = sum(1, 'b');
        expect(resultOfSum).toBeNaN();
    })
});
