import clicker, {initialState} from "./clicker";


describe('Reducer "clicker" works', () => {

    test('should return the init state', ()=> {
        expect(
            clicker()
        ).toEqual(initialState);
    });

    test('should change "clicked" 1 time ', () => {
        // expect(
        //     clicker(initialState,{type: 'INCREMENT'})
        // ).toEqual({clicked: 1});

        expect(
            clicker(initialState,{type: 'INCREMENT'})
        ).toEqual({...initialState, clicked: initialState.clicked + 1});
    })

    test('should change "clicked" 4 time ', () => {
        // expect(
        //     clicker(initialState,{type: 'INCREMENT', payload: {times: 4}})
        // ).toEqual({clicked: 4});

        expect(
            clicker(initialState,{type: 'INCREMENT', payload: {times: 4}})
        ).toEqual({...initialState, clicked: initialState.clicked + 4});
    })
})