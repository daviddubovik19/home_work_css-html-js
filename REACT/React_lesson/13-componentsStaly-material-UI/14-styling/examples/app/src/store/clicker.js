export const initialState = {
    clicked: 0,
    counter: 0
}

export default function clicker(
    state = initialState,
    action
) {

    const times = action && action.payload && action.payload.times ? action.payload.times : 1;

    switch (action && action.type) {
        case 'INCREMENT':
            return {...state, clicked: state.clicked + times}
        case 'DECREMENT':
            return {...state, clicked: state.clicked - times}
        case 'RESET':
            return initialState
        default:
            return state;
    }
}