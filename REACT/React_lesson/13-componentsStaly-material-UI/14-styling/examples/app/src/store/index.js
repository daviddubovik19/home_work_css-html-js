import {combineReducers, configureStore} from "@reduxjs/toolkit";
import storage from 'redux-persist/lib/storage'
import {persistReducer, persistStore} from "redux-persist";

import thunk from 'redux-thunk';
import logger from './Middleware/logger'

import clicker from "./clicker";
import counter from "./counterSlice"
import listReducer from "./listReducer";
import pageReducer from "./pageReducer";

const persistConfig = {
    key: 'root',
    storage
}

const allReducers = combineReducers({
    clicker: clicker,
    list: listReducer,
    counter: counter,
    pages: pageReducer,
});

const persistedReducer = persistReducer(persistConfig, allReducers);

const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk, logger]
});

export const persistor = persistStore(store);

export default store;