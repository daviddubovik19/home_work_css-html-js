import {fireEvent, render, screen} from "@testing-library/react";
import App from "./App";

describe('App test', () => {

    test('Main page works', () => {
        const view = render(<App />);

        const h1 = document.querySelector('h1');

        expect(h1.textContent).toBe('Home Page');
    });

    test('useReducer Example page works', () => {
        const view = render(<App />);

        const link = document.querySelector('a[href="/use-reducer"]');
        fireEvent.click(link);

        const h1 = document.querySelector('h1');

        expect(h1.textContent).toBe('useReducer Example page');

        const showButton = document.querySelector('button.show');
        const hideButton = document.querySelector('button.hide');

        expect(document.querySelector('.alert')).not.toBeTruthy();
        fireEvent.click(showButton);
        expect(document.querySelector('.alert')).toBeTruthy();
        fireEvent.click(hideButton);
        expect(document.querySelector('.alert')).not.toBeTruthy();

    });

});