import React from 'react';
import Header from './conponents/Header'
import Image from './conponents/image';
import logo from './img/logo.svg'
// конпонент
export default class App extends React.Component {
    helpText = 'Help text'

    render() {
        return (<div className='name'>
            <Header title='Шапка сайта'/>
            <Header title='Шапка сайта!'/>
            <Header title='!!!'/>
            <h1>{this.helpText}</h1>
            <input placeholder={this.helpText} onClick={this.inputClick} onMouseEnter={this.mouseOver}/>
            <p>{1 + 5}</p>
            <p>{this.helpText === 'Help text!' ? 'Yes' : 'No'}</p>
            <Image image={logo}/>
        </div>)
    }

    inputClick() {
        console.log('Clicked')
    }

    mouseOver() {
        console.log('mouse over')
    }

}