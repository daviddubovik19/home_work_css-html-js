import React from 'react';
import ReactDOMClient from 'react-dom/client';
import App from "./App";
import './css/style.css'


//
// <=================================>
// ReactDOM.render(
//     <div>
//         <h1>hello</h1>
//         <h2>привет</h2>
// </div>,document.getElementById('root'))

// <=================================>
// const inputClick = () => console.log('Clicked')
// const mouseOver = () => console.log('mouse over')

const app = ReactDOMClient.createRoot(document.getElementById('root'))
app.render(<App/>)