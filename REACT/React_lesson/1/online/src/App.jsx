
import './App.scss';
import React, { Component } from "react";


export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = { valueSet: Number(props.value) }
    this.onIncrement = this.onIncrement.bind(this)
    this.onDecrement = this.onDecrement.bind(this)
  }
  onIncrement() {
    this.setState({ valueSet: this.state.valueSet + 1})
 
  }
  onDecrement() {
    this.setState({ valueSet: this.state.valueSet - 1})
  }

  render() {
    const { valueSet } = this.state
    return (
      <>
        <button onClick={() => this.onIncrement()}>+</button>
        <p>{valueSet}</p>
        <button onClick={() => this.onDecrement()}>-</button>
      </>
    );
  }

}

