import {Link} from "react-router-dom";
function Favorite  (props){
        // console.log(props)
        return <Link to='Favorites' className='icon'> <img src="https://cdn-icons-png.flaticon.com/512/2550/2550288.png" alt='#'/> ({props.favorite.length})</Link>;
}
export default Favorite;