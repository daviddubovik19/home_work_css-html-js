import React, {useEffect} from "react";
import Button from "./component/Button";
import Modal from "./component/ModalWindow";
import CardList from "./component/CardList";
import BasketPages from "./pages/BasketPages";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Layout from "./pages/Layout";
import FavoritesPages from "./pages/FavoritesPages";
import {useDispatch, useSelector} from "react-redux";
import {loadProductsAsync} from "./store/reducer";

function App() {
    const isModalOne = useSelector(state => {
        return state.store.isModal1
    })
    const isModalTwo = useSelector(state => {
        return state.store.isModal2
    })
    const products = useSelector(state => {
        return state.store.products
    })
    const favoritesProducts = useSelector(state => {
        return state.store.favorites
    })
    const basketProduct = useSelector(state => {
        return state.store.basket
    })
    const currentProduct = useSelector(state => {
        return state.store.currentProduct
    })
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(loadProductsAsync())
    })
    const favorites = (product) => {
        let filterProduct = favoritesProducts.filter(res => product.id !== res.id)
        if (filterProduct.length === favoritesProducts.length) {
            filterProduct = [...favoritesProducts, product]
        }
        console.log(filterProduct)
        dispatch({type:'ADD_FAVORITES', payload: {favorite:favoritesProducts, filterProduct}})
        const productIds = filterProduct.map(({id}) => {
            return id
        })
        localStorage.setItem('favorites', JSON.stringify(productIds))
    }
    const addCard = (product) => {
        console.log(product)
        let filterProduct = basketProduct.filter(res => product.id !== res.id)
        if (filterProduct.length === basketProduct.length || basketProduct.length === 0) {
            dispatch({type:'ADD_BASKET', payload: {basket: basketProduct, filterProduct}})
            localStorage.setItem('basket', JSON.stringify([...basketProduct, product]))
        }
    }
    const cleanAdd = (product) => {
        let filterProduct = basketProduct.filter(res => product.id !== res.id)
        dispatch({type: 'ADD_BASKET', payload: {basket: filterProduct}})
        localStorage.setItem('basket', JSON.stringify(filterProduct))
    }
    const setCurrentProduct = (product, indexModal) => {
        console.log(product)
        dispatch({type: 'MODAL', payload: indexModal})
        dispatch({type: 'ADD_CURRENTPRODUCT', payload: {current: product}})

    }

    return (< >
        {isModalTwo && <Modal header='успіх ' closeButton={true} text=' товар був успішно добавлен в кошик'
                              close={() => dispatch({type: 'MODAL', payload: 'isModal2'})} actions={<div>
            <Button text='ok' class='ok' backgroundColor={'#b3382c'}
                    onClick={() => {

                        addCard(currentProduct)
                        dispatch({type: 'MODAL', payload: 'isModal2'})

                    }}/>
            <Button text='close' class="close" backgroundColor={'#b3382c'}
                    onClick={() => dispatch({type: 'MODAL', payload: 'isModal2'})}/>
        </div>}/>}

        {isModalOne && <Modal header='успіх ' closeButton={true} text=' товар був успішно видалиний в кошик'
                              close={() => dispatch({type: 'MODAL', payload: 'isModal1'})} actions={<div>
            <Button text='ok' class='ok' backgroundColor={'#b3382c'}
                    onClick={() => {
                        cleanAdd(currentProduct)
                        dispatch({type: 'MODAL', payload: 'isModal1'})
                    }}/>
            <Button text='close' class="close" backgroundColor={'#b3382c'}
                    onClick={() => dispatch({type: 'MODAL', payload: 'isModal1'})}/>
        </div>}/>}


        <BrowserRouter>
            <Routes>
                <Route path="/"
                       element={<Layout basket={basketProduct} favorites={favoritesProducts} cleanAdd={cleanAdd}/>}>
                    <Route path='products' element={<CardList products={products} favorites={favorites}
                                                              setCurrentProduct={(product) => setCurrentProduct(product, 'isModal2')}

                                                              stateFavorites={favoritesProducts}/>}/>
                    <Route path="Basket" element={<BasketPages basket={basketProduct}
                                                               setCurrentProduct={(product) => setCurrentProduct(product, 'isModal1')}/>}/>
                    <Route path="Favorites"
                           element={<FavoritesPages favorite={favoritesProducts} favoritesHandler={favorites}
                                                    setCurrentProduct={(product) => setCurrentProduct(product, 'isModal2')}/>}/>
                </Route>
            </Routes>
        </BrowserRouter>


    </>)

}

export default App