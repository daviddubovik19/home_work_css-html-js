import React from 'react';

import PropTypes, {func} from "prop-types";
import Basket from "../component/Basket";
import CardList from "../component/CardList";

const BasketPages = (props) => {
    return (
        <div className='Basket'>
            {props.basket <= 0  ? <h1>Кошик порожній</h1>: ''}
            <CardList products={props.basket}  setCurrentProduct={props.setCurrentProduct}   isBasket ={true}/>
            {/*{props.basket.map((el,index)=>{*/}
            {/*    return   <div key={index} className='added'>*/}
            {/*        <img src={el?.img_url} alt='#'/>*/}
            {/*        <h5>  {el?.name}</h5>*/}
            {/*        <p>ціна: {el?.price}</p>*/}
            {/*        <Button class='button-x' text='X' onClick={()=> {*/}
            {/*            props.clean(el)*/}
            {/*            props.close('isModal1')*/}
            {/*        }*/}
            {/*        }/>*/}
            {/*    </div>*/}
            {/*})}*/}
        </div>
    );
};

Basket.defaultProps = {
    add: [],
    clean: func
};

Basket.propTypes = {
    add: PropTypes.array,
    clean:PropTypes.func
};

export default BasketPages;