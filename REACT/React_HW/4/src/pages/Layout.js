import React from 'react';
import Basket from "../component/Basket";
import Favorite from "../component/Favorite";
import {Link, Outlet} from "react-router-dom";
import {useSelector} from "react-redux";


const Layout = (props) => {
    const basketProduct = useSelector(state => {
        return state.store.basket
    })
    const favoritesProduct = useSelector(state => {
        return state.store.favorites
    })
    // console.log( favoritesProduct)
    return (
        <>
            <header>
                <div className='block-products'>
                    <Link to='products' className='products'>Продукти</Link>
                    <div className="basket">
                        <Basket basket={basketProduct} clean={props.cleanAdd}/>

                    </div>
                    <div className="selected">
                        <Favorite favorite={favoritesProduct}/>
                    </div>
                </div>
            </header>
            <Outlet/>
        </>



    );
};

export default Layout;