import React from 'react';
import {useFormik} from "formik";
import * as Yup from "yup";
const initialValues = {
    name: '',
    lastname: '',
    age: '',
    deliveryAddress: '',
    cellPhone: ''
}
const validationSchema = Yup.object().shape({
    name: Yup.string()
        .min(3, "Ваше Ім'я повинно бути більше ніж 3 букви")
        .max(8, "Ваше Ім'я повино бути меньше ніж 8 букв").required(),
    lastname: Yup.string().min(6, 'призвіще довжно бути більше ніж 6').max(20,'призвіще довжно бути меньше ніж 20').required(),
    age: Yup.number().required(),
    deliveryAddress: Yup.string().required(),
    cellPhone: Yup.number().required()
})

const Formik = () => {
    const formik = useFormik({
        initialValues,
        validateOnBlur: true,
        validationSchema,



        onSubmit: values => {
            const prod = localStorage.getItem('basket')
            console.log(values, {prod})
            localStorage.removeItem('basket')

        }
    })
    return (
        <>
            <form noValidate  onSubmit={formik.handleSubmit } className='form'>
                <div>
                    <p>Ваше ім'я:{formik.touched.name && formik.errors.name }</p>
                    <input type='text' name='name' onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.name} />
                </div>
                <div>
                    <p>Ваше прізвище: {formik.touched.lastname && formik.errors.lastname }</p>
                    <input type='text' name='lastname'  onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.lastname}/>
                </div>
                <div>
                    <p>Скільки вам років:{formik.touched.age && formik.errors.age ? 'ваше вік в цифрах' : ''}</p>
                    <input type='text'  name='age'  onChange={formik.handleChange} onBlur={formik.handleBlur}value={formik.values.age}/>
                </div>
                <div>
                    <p>Ваша адрес: {formik.touched.deliveryAddress && formik.errors.deliveryAddress ? 'error': ''}</p>
                    <input type='text' name='deliveryAddress' value={formik.values.deliveryAddress} onChange={formik.handleChange} onBlur={formik.handleBlur}/>
                </div>
                <div>
                    <p>Ваш номер телефону:{formik.touched.cellPhone && formik.errors.cellPhone ? 'error':''}</p>
                    <input type='text' name='cellPhone' value={formik.values.cellPhone} onChange={formik.handleChange} onBlur={formik.handleBlur}/>
                </div>
                <button type='submit'>Checkout</button>
            </form>
        </>
    );
};

export default Formik;