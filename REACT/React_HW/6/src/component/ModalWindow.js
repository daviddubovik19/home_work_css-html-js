import Button from "./Button";
import '../style.scss'
import PropTypes, {element, func} from "prop-types";


function Modal(props) {

    return (
        <div className="wrapper " >
            <div className="modal">
                <div className="modal-window">
                    <h1> {props.header}</h1>
                    <p>{props.text}</p>
                    {props.closeButton && <Button text='X' class="btn-close" onClick={props.close}/>}
                    {props.actions}

                </div>
                <div className="back-modal" onClick={props.close}></div>
            </div>

        </div>
    )

}

Modal.defaultProps = {
    header: 'Stranger',
    text: 'Stranger',
    closeButton: true,
    actions: null,
    close: func
};

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    closeButton: PropTypes.bool,
    actions: PropTypes.element,
    close: PropTypes.func
};

export default Modal