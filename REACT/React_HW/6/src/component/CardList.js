import Card from "./Card";
import '../style.scss'
import PropTypes, {array} from "prop-types";
import {Context} from "./Context";
import {useContext} from "react";


function CardList(props) {
    const {kindProduct} = useContext(Context)
    const favStateId = props.stateFavorites ? props.stateFavorites.map(({id}) => {
        return id
    }) : []

    const renderCards = props.products.map((el, index) => {
        return <Card product={el} key={index} favorites={props.favorites} setCurrentProduct={props.setCurrentProduct}

                     isfavorites={favStateId.includes(el.id)} isBasket={props.isBasket}
        />
    })
    return (
        <ul className={kindProduct ? 'tebles' : 'Cards'}>
            {renderCards}
        </ul>
    );
}


CardList.defaultProps = {
    products: []
};

CardList.propTypes = {
    products: PropTypes.array
};

export default CardList;