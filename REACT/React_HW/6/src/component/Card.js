import Button from "./Button";
import '../style.scss'
import PropTypes, { object } from "prop-types";
import {Context} from "./Context";
import {useContext} from "react";


function Card(props) {
    const {kindProduct} = useContext(Context)
    const icon = props.isfavorites ? "https://cdn-icons-png.flaticon.com/512/2550/2550288.png" : 'https://cdn-icons-png.flaticon.com/128/8451/8451422.png'
 
    return (
        <li className={kindProduct ? 'teble' : 'Card'}>
            <img src={props.product.img_url} alt='#' />
            <p>{props.product.name}</p>
            <p>{props.product.decription}</p>
            <p>{props.product.price}</p>

            {
                !props.isBasket &&
                <Button text={<img src={icon} alt='#' />} class='add-selected'
                    onClick={() => props.favorites(props.product)} />
            }


            <Button className='button-x' text={!props.isBasket ? 'Add to cart' : 'delete'} onClick={() => {
                props.setCurrentProduct(props.product)
            }} />
        </li>
    );

}

Card.defaultProps = {
    products: {}
};

Card.propTypes = {
    products: PropTypes.object
};

export default Card;