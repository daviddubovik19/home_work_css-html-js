
import PropTypes, {func, string} from "prop-types";


function Button(props) {
    return (
        <button className={props.class} style={{backgroundColor: props.backgroundColor}}
                onClick={props.onClick}>{props.text} </button>
    );

}

Button.defaultProps = {
    class: "Stranger",
    backgroundColor: 'Stranger',
    onClick: func
};

Button.propTypes = {
    class: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func
};
export default Button;
