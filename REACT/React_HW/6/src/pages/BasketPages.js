import React, {useContext} from 'react';
import PropTypes, {func} from "prop-types";
import Basket from "../component/Basket";
import CardList from "../component/CardList";
import Formik from "../component/Formik";
import {Context} from "../component/Context";


const BasketPages = (props) => {
    const {kindProduct, setKindProduct} = useContext(Context)
    // console.log(kindProduct)
    return (
        <>
            <div className='products-basket'>
                <button onClick={()=> setKindProduct()} className='button-switch'>{kindProduct ?  'table':'card'}</button>
                {props.basket <= 0 ? <h1>Кошик порожній</h1> : ''}
                <CardList products={props.basket} setCurrentProduct={props.setCurrentProduct} isBasket={true}/>
            </div>
            <Formik/>
        </>

    );
};

Basket.defaultProps = {
    add: [],
    clean: func
};

Basket.propTypes = {
    add: PropTypes.array,
    clean: PropTypes.func
};

export default BasketPages;