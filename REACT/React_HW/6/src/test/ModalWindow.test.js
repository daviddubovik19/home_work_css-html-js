import React from "react";
import Modal from "../component/ModalWindow";
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom'

describe('modal it', () => {
    it('modal text', () => {
        render(<Modal text="test" />);
        const p = screen.getByRole("heading");
        expect(p).toBeInTheDocument();
    })
    it("button X", () => {
        const { getByRole } = render(<Modal closeButton= {true} />);
        const buttonEl = getByRole("button");
        expect(buttonEl).toBeInTheDocument();
    });
})