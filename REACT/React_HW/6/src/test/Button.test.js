import React from "react";
import Button from "../component/Button";
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom'

describe('button it', () => {
    it('button text', () => {
        render(<Button text="test" />);
        const button = screen.getByRole('button');
         expect(button).toBeInTheDocument();
    })
    it("test button prop style", () => {
        const { getByRole } = render(<Button backgroundColor="red" />);
        const buttonEl = getByRole("button");
        expect(buttonEl).toHaveStyle("background-color: red");
        expect(buttonEl).toBeInTheDocument;
    });
    it("test modal func", () => {
        const mockedOnStepClick = jest.fn();
        const { getByRole } = render(<Button onClick={mockedOnStepClick} />);
        fireEvent.click(getByRole("button"));
        expect(mockedOnStepClick).toHaveBeenCalledTimes(1);
    });

})