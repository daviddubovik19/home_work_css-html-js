import React from "react";
import reducer from "../store/reducer";
import '@testing-library/jest-dom'
import data from '../../public/data/data.json'
import 'jest-localstorage-mock';

// beforeEach(() => {
//     localStorage.clear()
// });
// afterEach(() => {
//     localStorage.setItem('favorites', JSON.stringify(data.products));
//
// });
describe('reducer test', () => {
    it('test state ', () => {
        const mockDataExpect = {
            "isModal1": true
        }
        expect(reducer({}, {type: 'MODAL', payload: 'isModal1'})).toStrictEqual(mockDataExpect)
    });

    // it("localStorage ", () => { // тест не працює, не зберігає в localStorage
    //     // jest.spyOn(localStorage, 'favorites');
    //     // const result = '{ "name":"John", "age":30, "car":"bmw"}';
    //     // global.localStorage.setItem('favorites', JSON.stringify(result));
    //    const data = localStorage.getItem('favorites')
    //     expect(reducer({}, {type: 'PRODUCTS', payload: {product: []}})).toStrictEqual(data)
    // });


})


