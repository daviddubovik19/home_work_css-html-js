import React from 'react';
import Basket from "../component/Basket";
import Card from "../component/Card";
import Favorite from "../component/Favorite";
import Formik from "../component/Formik";
import Modal from "../component/ModalWindow";
describe('Components', () => {
    it('Basket', () => {
        const tree = (<Basket basket="John" />);
        expect(tree).toMatchSnapshot();
    });
    it('Card', () => {
       const tree = (<Card />);
            expect(tree).toMatchSnapshot();
    });
    it('Favorite', () => {
      const tree = (<Favorite />);
            expect(tree).toMatchSnapshot();
    });
    it('Formik', () => {
      const tree = (<Formik />);
            expect(tree).toMatchSnapshot();
    });
    it('Modal', () => {
      const tree = (<Modal />);
            expect(tree).toMatchSnapshot();
    });
});