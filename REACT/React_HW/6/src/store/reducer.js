const initialState = {
    isModal1: false,
    isModal2: false,
    products: [],
    favorites: [],
    basket: [],
    currentProduct: null,
    kind: false
}
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case 'MODAL':
            return {...state, [action.payload]: !state[action.payload],}
        case 'PRODUCTS':
            const defaultFavorite = localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : []
            const productsBasket = localStorage.getItem('basket') ? JSON.parse(localStorage.getItem('basket')) : []
            const filterProduct =   action.payload.product.filter(res => defaultFavorite.includes(res.id))
            // console.log(action.payload.product)
            return {...state, products: action.payload.product, favorites: filterProduct, basket: productsBasket}
        case 'ADD_FAVORITES':
            return {...state, favorites: action.payload.favorite}
        case 'ADD_BASKET':
            return {...state, products: action.payload.basket}
        case'ADD_CURRENTPRODUCT':
            return {...state, currentProduct: action.payload.current}
        case 'KIND_PRODUCT':
            return {...state, kind: action.payload}
        default:
            return {...state}
    }

}
export const loadProductsAsync = () => {
    return async (dispatch) => {
        const response = await fetch('data/data.json')
        const {products} = await response.json();
        dispatch({type: 'PRODUCTS', payload: {product: products}})

    }
}
