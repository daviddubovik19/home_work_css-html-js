import React, {Component} from "react";
import Button from "./component/Button";
import Modal from "./component/ModalWindow";
import CardList from "./component/CardList";
import Basket from "./component/Basket";
import Favorite from "./component/Favorite";

// import PropTypes from "prop-types";


class App extends Component {
    constructor() {
        super();
        this.state = {
            isModal2: false,
            products: [],
            favorites: [],
            basket: []
        }
        this.favorites = this.favorites.bind(this)
        this.addCard = this.addCard.bind(this)
        this.close = this.close.bind(this)
    }

    componentDidMount() {
        fetch('data/data.json')
            .then(res => res.json())
            .then(data => this.setState((prev) => {

                const defaultFavorite = localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : []
                const productsBasket = localStorage.getItem('basket') ? JSON.parse(localStorage.getItem('basket')) : []
                // console.log(defaultFavorite)
                // console.log(productsBasket)
                // console.log(data.products.filter(res =>  defaultFavorite.includes(res.id)))
                // console.log(data.products.filter(res => res.id)) // не повертає id
                // console.log(data.products.id) //undefined
                // console.log(data.products)
                // console.log( defaultFavorite.includes(data.products.id)) //не працює
                // const w = data.products.filter(res => defaultFavorite.includes(res.id))
                // console.log(w)

                return {
                    ...prev,
                    products: data.products,
                    favorites: data.products.filter(res => defaultFavorite.includes(res.id)),
                    basket: productsBasket
                }
            }))

    }

    close(isModal) {
        this.setState({
            ...this.state,
            [isModal]: !this.state[isModal],
        })
    }

    favorites(product) {
        console.log(product)
        // console.log(this.state.favorites)
        let filterProduct = this.state.favorites.filter(res => product.id !== res.id)
        console.log(filterProduct)
        // console.log(`filter:${ filterProduct.length}`)
        // console.log(`state:${this.state.favorites.length}`)
        if (filterProduct.length === this.state.favorites.length) {
            filterProduct = [...this.state.favorites, product]
        }
        // console.log(filterProduct)
        this.setState({favorites: filterProduct})
        const productIds = filterProduct.map(({id}) => {
            return id
        })
        // console.log(productIds)
        localStorage.setItem('favorites', JSON.stringify(productIds))


        // const copyFavorites = [...this.state.favorites];
        // const indexProduct = copyFavorites.findIndex(currentProduct => currentProduct.id === product.id);
        // const newFavorites = indexProduct === -1 ? [...copyFavorites, product] : (copyFavorites.splice(indexProduct, 1), copyFavorites);
        // this.setState({...this.state, favorites:newFavorites})


    }

    addCard(product) {
        const filterProduct = this.state.basket.filter(res => product.id !== res.id)
        if (filterProduct.length === this.state.basket.length) {
            this.setState({basket: [...this.state.basket, product]})
            localStorage.setItem('basket', JSON.stringify([...this.state.basket, product]))
        }
    }
    cleanAdd(key){
        console.log(key)
    }
    render() {
        return (
            < >
                {this.state.isModal2 &&
                    <Modal header='успіх ' closeButton={true} text=' товар був успішно добавлен в кошик'
                           close={() => this.close('isModal2')} actions={
                        <div>
                            <Button text='ok' class='ok' backgroundColor={'#b3382c'}
                                    onClick={() => this.close('isModal2')}/>
                            <Button text='close' class="close" backgroundColor={'#b3382c'}
                                    onClick={() => this.close('isModal2')}/>
                        </div>
                    }/>}

                <CardList products={this.state.products} favorites={this.favorites} add={this.addCard}
                          close={this.close} stateFavorites={this.state.favorites}/>
                <div>
                    <div className="basket">
                        <Basket add={this.state.basket} clean={this.cleanAdd}/>

                    </div>
                    <div className="selected">
                        <Favorite favorite={this.state.favorites}/>
                    </div>
                </div>


            </>
        )
    }
}

export default App