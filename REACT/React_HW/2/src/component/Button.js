import {Component} from "react";
import PropTypes, {func, string} from "prop-types";


class Button extends Component {
    constructor(props) {
        super()
    }

    render() {
        return (
            <button className={this.props.class} style={{backgroundColor: this.props.backgroundColor}}
                    onClick={this.props.onClick}>{this.props.text} </button>
        );
    }
}

Button.defaultProps = {
    class: "Stranger",
    backgroundColor: 'Stranger',
    onClick: func
};

Button.propTypes = {
    class: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func
};
export default Button;
