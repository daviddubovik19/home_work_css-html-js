import Button from "./Button";
import { Component } from "react";
import '../style.scss'
import PropTypes, {element, func} from "prop-types";


class Modal extends Component {

    render() {

        return (
            <div className="wrapper ">
                <div className="modal">
                    <div className="modal-window">
                        <h1> {this.props.header}</h1>
                        <p>{this.props.text}</p>
                        {this.props.closeButton && <Button text='X' class="btn-close" onClick={this.props.close} />}
                        {this.props.actions}
                        
                    </div>
                    <div className="back-modal" onClick={this.props.close}></div>
                </div>

            </div>
        )
    }
}
Modal.defaultProps = {
header:'Stranger',
    text:'Stranger',
    closeButton: true,
    actions: element,
    close: func
};

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    closeButton: PropTypes.bool,
    actions:PropTypes.element,
    close:PropTypes.func
};

export default Modal