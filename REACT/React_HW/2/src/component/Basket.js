import React, {Component} from 'react';
import PropTypes, {func,array} from "prop-types";
import Button from "./Button";
class Basket extends Component {
    render() {

        return (
            <>
                <p>кошик <img src="https://cdn-icons-png.flaticon.com/512/15/15278.png" alt="#"/></p>
                {this.props.add.map((el,index)=>{
                    return   <div key={index} className='added'>
                      <img src={el.img_url} alt='#'/>
                      <h5>  {el.name}</h5>
                        <p>ціна: {el.price}</p>
                        <Button text='X' onClick={()=> this.props.clean(el)}/>
                    </div>
                })}
            </>
        );
    }
}

Basket.defaultProps = {
    add: [],
    clean: func
};

Basket.propTypes = {
    add: PropTypes.array.isRequired,
    clean:PropTypes.func
};

export default Basket;