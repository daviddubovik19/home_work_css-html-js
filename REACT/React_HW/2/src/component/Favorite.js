import React, {Component} from 'react';
import PropTypes, {array} from "prop-types";



class Favorite extends Component {
    render() {
        return (
            <>
                <p>обране <img src="https://cdn-icons-png.flaticon.com/512/2550/2550288.png" alt='#'/></p>

                {this.props.favorite.map((el, index)=>{
                    return   <div key={index} className='added'>
                        <img src={el.img_url} alt='#'/>
                        <h5>  {el.name}</h5>
                        <p>ціна: {el.price}</p>
                    </div>
                })}
            </>
        );
    }
}

Favorite.defaultProps = {
    favorite: []
};

Favorite.propTypes = {
    favorite: PropTypes.array
};
export default Favorite;