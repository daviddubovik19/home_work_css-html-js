import React, {Component} from 'react';
import Card from "./Card";
import '../style.scss'
import PropTypes, {array} from "prop-types";


class CardList extends Component {

    render() {
        const favStetaId = this.props.stateFavorites.map(({id}) => {
            return id
        })
        const renderCards = this.props.products.map((el, index) => {
            return <Card product={el} key={index} favorites={this.props.favorites} add={this.props.add}
                         close={this.props.close}
                         isfavorites={favStetaId.includes(el.id)}
            />
        })
        return (
            <ul className='Cards'>
                {renderCards}
            </ul>
        );
    }
}

CardList.defaultProps = {
    products: []
};

CardList.propTypes = {
    products: PropTypes.array
};

export default CardList;