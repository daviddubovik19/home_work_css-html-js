import React, {Component} from 'react';
import Button from "./Button";
import '../style.scss'
import PropTypes, { object} from "prop-types";


class Card extends Component {
    render() {

        const icon = this.props.isfavorites ? "https://cdn-icons-png.flaticon.com/512/2550/2550288.png" : 'https://cdn-icons-png.flaticon.com/128/8451/8451422.png'
        return (
            <li className='Card'>
                <img src={this.props.product.img_url} alt='#'/>
                <p>{this.props.product.name}</p>
                <p>{this.props.product.decription}</p>
                <p>{this.props.product.price}</p>


                <Button text={<img src={icon} alt='#'/>} class='add-selected'
                        onClick={() => this.props.favorites(this.props.product)}/>

                <Button text={'Add to cart'} onClick={() => {
                    this.props.close('isModal2')
                    this.props.add(this.props.product)

                }}/>
            </li>
        );
    }
}

Card.defaultProps = {
products: {}
};

Card.propTypes = {
products: PropTypes.object
};

export default Card;