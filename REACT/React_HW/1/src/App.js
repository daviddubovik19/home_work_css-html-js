import React, { Component } from "react";
import Button from "./component/Button";
import Modal from "./component/modalWindow";

class App extends Component {
    constructor() {
        super();
        this.state = {
            isModal1: false,
            isModal2: false

        }
    }
    close(isModal) {
        this.setState({
            ...this.state,
            [isModal]: !this.state[isModal],
        })
    }
    render() { 
        return (
            <div>
                {this.state.isModal1 && <Modal header='one window' closeButton={true} text='randon text' close={() => this.close('isModal1')} actions={

                    <div>
                        <Button text='ok' class='ok' backgroundColor={'#b3382c'} onClick={() => this.close('isModal1')} />
                        <Button text='close' class="close" backgroundColor={'#b3382c'} onClick={() => this.close('isModal1')} />
                    </div>
                } />}
                {this.state.isModal2 && <Modal header='one ' closeButton={true} text=' text' close={() => this.close('isModal2')} actions={
                    <div>
                        <Button text='ok' class='ok' backgroundColor={'#b3382c'} onClick={() => this.close('isModal2')} />
                        <Button text='close' class="close" backgroundColor={'#b3382c'} onClick={() => this.close('isModal2')} />
                    </div> 
                } />}
                <Button text=' Open first modal' backgroundColor={'blue'} onClick={() => this.close('isModal1')} />
                <Button text=' Open second modal' backgroundColor={'yellow'} onClick={() => this.close('isModal2')} />
            </div>
        )
    }
}
export default App