import { Component } from "react";

class Button extends Component {
  constructor(props) {
    super()
  }
  render(){
    return (
      <button className={this.props.class} style={{backgroundColor:this.props.backgroundColor}} onClick={this.props.onClick} >{this.props.text} </button>
   );
  }
}
export default Button;
