import Button from "./Button";
// import App from "../App";
import { Component } from "react";
import '../style.scss'

class Modal extends Component {
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <div className="wrapper ">
                <div className="modal">
                    <div className="modal-window">
                        <h1> {this.props.header}</h1>
                        <p>{this.props.text}</p>
                        {this.props.closeButton && <Button text='X' class="btn-close" onClick={this.props.close} />}
                        {this.props.actions}
                        
                    </div>
                    <div className="back-modal" onClick={this.props.close}></div>
                </div>

            </div>
        )
    }
}


export default Modal