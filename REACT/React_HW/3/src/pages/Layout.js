import React from 'react';
import Basket from "../component/Basket";
import Favorite from "../component/Favorite";
import {Link, Outlet} from "react-router-dom";

const Layout = (props) => {
    return (
        <>
            <header>
                <div className='block-products'>
                    <Link to='products' className='products'>Продукти</Link>
                    <div className="basket">
                        <Basket basket={props.basket} clean={props.cleanAdd}/>

                    </div>
                    <div className="selected">
                        <Favorite favorite={props.favorites}/>
                    </div>
                </div>
            </header>
            <Outlet/>
        </>



    );
};

export default Layout;