import React from 'react';
import PropTypes from "prop-types";
import CardList from "../component/CardList";


const FavoritesPages = (props) => {
    return (
        <div className='favorite'>
            {props.favorite <= 0  ? <h1>Вибраного немає</h1>: ''}
            <CardList products={props.favorite} favorites={props.favoritesHandler}
                      setCurrentProduct={ props.setCurrentProduct}
                     stateFavorites={props.favorite}/>
            {/*{props.favorite.map((el, index)=>{*/}
            {/*    return   <div key={index} className='added'>*/}
            {/*        <img src={el.img_url} alt='#'/>*/}
            {/*        <h5>  {el.name}</h5>*/}
            {/*        <p>ціна: {el.price}</p>*/}
            {/*    </div>*/}
            {/*})}*/}
        </div>
    );
};
FavoritesPages.defaultProps = {
    favorite: []
};

FavoritesPages.propTypes = {
    favorite: PropTypes.array
};
export default FavoritesPages;