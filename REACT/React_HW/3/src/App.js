import React, {useEffect, useState} from "react";
import Button from "./component/Button";
import Modal from "./component/ModalWindow";
import CardList from "./component/CardList";
import Basket from "./component/Basket";
import Favorite from "./component/Favorite";
import BasketPages from "./pages/BasketPages";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Layout from "./pages/Layout";
import FavoritesPages from "./pages/FavoritesPages";

function App() {

    const [state, setState] = useState({
        isModal1: false,
        isModal2: false,
        products: [],
        favorites: [],
        basket: [],
        currentProduct: null
    })


    const close = (isModal) => {
        setState({
            ...state,
            [isModal]: !state[isModal],
        })
    }

    useEffect(() => {
        fetch('data/data.json')
            .then(res => res.json())
            .then(data => setState((prev) => {

                const defaultFavorite = localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : []
                const productsBasket = localStorage.getItem('basket') ? JSON.parse(localStorage.getItem('basket')) : []

                return {
                    ...prev,
                    products: data.products,
                    favorites: data.products.filter(res => defaultFavorite.includes(res.id)),
                    basket: productsBasket
                }
            }))

    })

    const favorites = (product) => {

        let filterProduct = state.favorites.filter(res => product.id !== res.id)

        if (filterProduct.length === state.favorites.length) {
            filterProduct = [...state.favorites, product]
        }
        setState({...state, favorites: filterProduct})
        const productIds = filterProduct.map(({id}) => {
            return id
        })
        localStorage.setItem('favorites', JSON.stringify(productIds))


    }

    const addCard = (product) => {
        let filterProduct = state.basket.filter(res => product.id !== res.id)
        if (filterProduct.length === state.basket.length || state.basket.length === 0) {
            setState({...state, basket: [...state.basket, product]})
            localStorage.setItem('basket', JSON.stringify([...state.basket, product]))
        }

    }


    const cleanAdd = (product) => {

        let filterProduct = state.basket.filter(res => product.id !== res.id)
        setState({...state, basket: filterProduct})
        localStorage.setItem('basket', JSON.stringify(filterProduct))
    }
    const setCurrentProduct = (product, indexModal) => {
        close(`isModal${indexModal}`)
        setState((prev) => ({...prev, currentProduct: product}))
    }
    return (
        < >
            {state.isModal2 &&
                <Modal header='успіх ' closeButton={true} text=' товар був успішно добавлен в кошик'
                       close={() => close('isModal2')} actions={
                    <div>
                        <Button text='ok' class='ok' backgroundColor={'#b3382c'}
                                onClick={() => {

                                    state.currentProduct && console.log(state.currentProduct)
                                    addCard(state.currentProduct)
                                    close('isModal2')

                                }}/>
                        <Button text='close' class="close" backgroundColor={'#b3382c'}
                                onClick={() => close('isModal2')}/>
                    </div>
                }/>}

            {state.isModal1 &&
                <Modal header='успіх ' closeButton={true} text=' товар був успішно видалиний в кошик'
                       close={() => close('isModal1')} actions={
                    <div>
                        <Button text='ok' class='ok' backgroundColor={'#b3382c'}
                                onClick={() => {
                                    cleanAdd(state.currentProduct)
                                    close('isModal1')
                                }}/>
                        <Button text='close' class="close" backgroundColor={'#b3382c'}
                                onClick={() => close('isModal1')}/>
                    </div>
                }/>}





            <BrowserRouter >
                <Routes>
                    <Route path="/" element={<Layout basket={state.basket} favorites={state.favorites} cleanAdd={cleanAdd}/>}>
                        <Route path='products' element={<CardList products={state.products} favorites={favorites}
                                                        setCurrentProduct={(product)=> setCurrentProduct(product,2)}
                                                        close={close} stateFavorites={state.favorites}/>}/>
                        <Route path="Basket" element={<BasketPages basket={state.basket}   close={close}   setCurrentProduct={(product)=> setCurrentProduct(product,1)}/> }/>
                        <Route path="Favorites" element={<FavoritesPages favorite={state.favorites} favoritesHandler={favorites} setCurrentProduct={(product)=> setCurrentProduct(product,2)}/>}/>

                    </Route>
                </Routes>
            </BrowserRouter>


        </>
    )

}

export default App