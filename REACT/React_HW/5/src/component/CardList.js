import Card from "./Card";
import '../style.scss'
import PropTypes, {array} from "prop-types";


function CardList (props) {
        const favStateId = props.stateFavorites ? props.stateFavorites.map(({id}) => {
            return id
        }): []
    // console.log(props.products)
    // console.log(props.products.filterProduct)
        const renderCards = props.products.map((el, index) => {
            return <Card product={el} key={index} favorites={props.favorites} setCurrentProduct={props.setCurrentProduct}

                         isfavorites={favStateId.includes(el.id)} isBasket={props.isBasket}
            />
        })
        return (
            <ul className='Cards'>
                {renderCards}
            </ul>
        );
    }


CardList.defaultProps = {
    products: []
};

CardList.propTypes = {
    products: PropTypes.array
};

export default CardList;