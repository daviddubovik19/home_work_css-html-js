import React from 'react';
import PropTypes from "prop-types";
import CardList from "../component/CardList";
import {useSelector} from "react-redux";


const FavoritesPages = (props) => {
    const favoritesProduct = useSelector(state => {
        return state.store.favorites
    })
    return (
        <div className='favorite'>
            {favoritesProduct <= 0  ? <h1>Вибраного немає</h1>: ''}
            <CardList products={favoritesProduct} favorites={props.favoritesHandler}
                      setCurrentProduct={ props.setCurrentProduct}
                     stateFavorites={props.favorite}/>

        </div>
    );
};
FavoritesPages.defaultProps = {
    favorite: []
};

FavoritesPages.propTypes = {
    favorite: PropTypes.array
};
export default FavoritesPages;