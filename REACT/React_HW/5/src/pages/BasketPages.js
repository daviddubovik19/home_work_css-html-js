import React from 'react';
import PropTypes, {func} from "prop-types";
import Basket from "../component/Basket";
import CardList from "../component/CardList";
import Formik from "../component/Formik";


const BasketPages = (props) => {


    return (
        <>
            <div className='products-basket'>
                {props.basket <= 0 ? <h1>Кошик порожній</h1> : ''}
                <CardList products={props.basket} setCurrentProduct={props.setCurrentProduct} isBasket={true}/>
            </div>
            <Formik/>
        </>

    );
};

Basket.defaultProps = {
    add: [],
    clean: func
};

Basket.propTypes = {
    add: PropTypes.array,
    clean: PropTypes.func
};

export default BasketPages;