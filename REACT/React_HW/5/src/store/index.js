import reducer from './reducer'
import {configureStore} from "@reduxjs/toolkit";
import thunk from "redux-thunk";

const store = configureStore({
    reducer: {store: reducer},
    middleware:[thunk]
})

export default store