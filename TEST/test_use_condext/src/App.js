
import ListComponent from './context';
import React, {useState} from "react";
import Component from "./component";


function App() {
    const [state, setState] = useState(false)
    const set = ()=>{
        setState(!state)
        console.log(state)
    }
  return (
      <ListComponent.Provider value={{state,set}}>
          <div className="App">
              <Component/>
          </div>
      </ListComponent.Provider>

  );
}

export default App;
