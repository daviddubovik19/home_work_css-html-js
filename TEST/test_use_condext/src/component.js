import React from 'react';
import {useContext} from "react";
import ListComponent from "./context";

const Component = () => {
    const {set,state}= useContext(ListComponent)
    return (
        <div>
           <button onClick={() => set()}>click:{state}</button>
        </div>
    );
};

export default Component;