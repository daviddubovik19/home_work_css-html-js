import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import ParentComponent from './component/ParentComponent';
import MyComponent from './component/dropList';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
    <MyComponent />
  </React.StrictMode>
);

