import React, { useState } from 'react';
import Dropdown from './dropdown';

const SpecialWindow = (props) => {
  const { options } = props;

  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState('');

  const handleOptionSelected = (event) => {
    setSelectedOption(event.target.value);
    setIsOpen(false);
  };

  return (
    <div>
      <button onClick={() => setIsOpen(!isOpen)}>Open Window</button>
      {isOpen && (
        <div>
          <Dropdown
            options={options}
            selectedOption={selectedOption}
            onOptionSelected={handleOptionSelected}
          />
        </div>
      )}
    </div>
  );
};

export default SpecialWindow;
