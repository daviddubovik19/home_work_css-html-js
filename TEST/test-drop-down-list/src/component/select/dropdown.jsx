import React from 'react';

const Dropdown = (props) => {
  const { options, selectedOption, onOptionSelected } = props;

  return (
    <select value={selectedOption} onChange={onOptionSelected}>
      {options.map(option => (
        <option key={option.value} value={option.value}>{option.label}</option>
      ))}
    </select>
  );
};

export default Dropdown;
