import React from 'react';
import SpecialWindow from './SpecialWindow';

const options = [
  { value: 'option1', label: 'Option 1' },
  { value: 'option2', label: 'Option 2' },
  { value: 'option3', label: 'Option 3' },
];

const ParentComponent = () => {
  return (
    <div>
      <SpecialWindow options={options} />
    </div>
  );
};

export default ParentComponent;
