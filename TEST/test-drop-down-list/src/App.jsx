import React, { useState } from 'react';
import './App.css';

function App() {
  const [list, setList] = useState(false)
  const default_ul = () => {
    return (
      < ul className='default_option' onClick={() => setList(!list)} >
        <li>
          <div className="option "  >
            <div className="icon"></div>
            <p>Pizzas</p>
          </div>
        </li>
      </ul >
    )
  }




  return (
    <>
      <div className={`select_wrap  ${list ? 'active' : ''}`}>

        {default_ul()}

        <ul className="select_ul" >
          <li onClick={() => {
            default_ul()
            setList(!list)
          }}>
            <div className="option pizza" >
              <div className="icon"></div>
              <p>w</p>
            </div>
          </li>
        </ul>

      </div>
    </>
  );
}

export default App;
